## Pertaining to Time Complexity, what is the average case Big\(O\) for the merge sort algorithm for an array containing n elements?

KSATs: K0135

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | ***O\(n\*log\(n\)\)*** | O\(n\*n\) |


Feedback:

- The average case sorting for merge sort is O\(n\*log\(n\)\)

[**<- Back To README**](../../README.md)

