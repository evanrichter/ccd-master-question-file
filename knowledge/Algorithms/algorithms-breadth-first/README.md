```mermaid
graph TD
  A-->B
  A-->C
  B-->D
  B-->E
  C-->F
  C-->G

```

## In the above graph, using a breadth\-first traversal, which node would be searched next given the current search was on node B?

KSATs: K0047

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***C*** | D | E | A |


Feedback:

- A breadth\-first traversal will visit each node in a tree at the same level before moving down in the tree to the next level\.

[**<- Back To README**](../../README.md)

