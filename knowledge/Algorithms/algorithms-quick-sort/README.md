## Pertaining to Time Complexity, what is the average case Big\(O\) for the quick sort algorithm for an array containing n elements?

KSATs: K0137

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | ***O\(n\*log\(n\)\)*** | O\(n\*n\) |


Feedback:

- The average case sorting for quick sort is O\(n\*log\(n\)\)

[**<- Back To README**](../../README.md)

