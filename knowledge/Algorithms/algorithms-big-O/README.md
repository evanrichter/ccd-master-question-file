## In computer science, what term is used for the notation to classify algorithms according to how their running time or space requirements grow as the input size grows?

KSATs: K0132

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Capacity measure | ***Big O notation*** | Complexity rating | Weighted measure |


Feedback:

- Big\-O is the common term used to classify a sorting or searching algorithm according to the size of the data structure being used\.

[**<- Back To README**](../../README.md)

