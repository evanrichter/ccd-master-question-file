## Pertaining to Time Complexity, what is the average case Big\(O\) for inserting, searching, or deleting from a hash table?

KSATs: K0139

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***O\(1\)*** | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |


Feedback:

- The average case for using a hash table to insert, search, or delete is O\(1\)

[**<- Back To README**](../../README.md)

