```mermaid
graph TD
  A-->B
  A-->C
  B-->D
  B-->E
  C-->F
  C-->G

```

## Referring to the above depicted graph, using a depth\-first traversal, which node would be searched next given the current search was on node B?

KSATs: K0046

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| C | D | E | ***D or E depending on implementation*** |


Feedback:

- A depth\-first traversal will visit each node in a branch of a tree before moving to the next branch\. If the branch splits, it depends on the specific algorithm implementation on which branch to go down\.

[**<- Back To README**](../../README.md)

