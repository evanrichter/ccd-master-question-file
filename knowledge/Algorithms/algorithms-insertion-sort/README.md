## Pertaining to Time Complexity, what is the average case Big\(O\) for the insertion sort algorithm for an array containing n elements?

KSATs: K0133

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | ***O\(n\*n\)*** |


Feedback:

- The average case sorting for insertion sort is O\(n\*n\)

[**<- Back To README**](../../README.md)

