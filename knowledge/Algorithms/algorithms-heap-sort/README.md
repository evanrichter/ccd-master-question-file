## Pertaining to Time Complexity, what is the average case Big\(O\) for the heap sort algorithm for an array containing n elements?

KSATs: K0136

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | ***O\(n\*log\(n\)\)*** | O\(n\*n\) |


Feedback:

- The average case sorting for heap sort is O\(n log n\)

[**<- Back To README**](../../README.md)

