## What term is used for byte ordering where the least significant bytes are stored first in a memory location?

KSATs: K0210, K0204, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| last in first out | ***little endian*** | big endian | first in first out |


Feedback:

- A) Incorrect: last in first out is a term typically used for a queue construct for data coming in and out of the queue
- B) Correct: With little endian, the least significant byte of the data is placed at the byte with the lowest address
- C) Incorrect: With big endian, The most significant byte of the data is placed at the byte with the lowest address\.
- D) Incorrect: first in first out is a term typically used for a stack construct for data coming in and out of the stack

[**<- Back To README**](../../README.md)

