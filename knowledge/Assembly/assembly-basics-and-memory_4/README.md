## Which of the following is a general purpose register?

KSATs: K0201, K0202, K0204, K0528, K0691

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| ebp | rri | esp | ***rdx*** |


Feedback:

- A) Incorrect: ebp is not a general purpose register, it is a base pointer register
- B) Incorrect: rri is not a valid assembly register name
- C) Incorrect: esp is not a general purpose register, it is a stack pointer register
- D) Correct: rdx is the standard general purpose register in x86\_64 assembly

[**<- Back To README**](../../README.md)

