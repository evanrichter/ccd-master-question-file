## What best describes the x86/x86\-64 movs instruction?

KSATs: K0201, K0691, K0767

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Copy the item from the second operand into the first operand\. | Move the item from the second operand into the first operand and set the second operand to zero\. | ***Copy the item from the second operand into the first operand and change edi and esi\.*** | Move only one bit from the second operand into the first\. |


Feedback:

- movs will copy from the source to the destination and increment or decrement edi and esi automatically\.

[**<- Back To README**](../../README.md)

