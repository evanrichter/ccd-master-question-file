## In Assembly, to put data into a register, you use the \_\_\_\_\_ opcode\.

KSATs: K0204, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| ins | ***mov*** | equ | asn |


Feedback:

- A) Incorrect: ins is not a valid assembly opcode
- B) Correct: mov is the correct opcode to move a value into a register
- C) Incorrect: equ is not a valid assembly opcode
- D) Incorrect: asn is not a valid assembly opcode

[**<- Back To README**](../../README.md)

