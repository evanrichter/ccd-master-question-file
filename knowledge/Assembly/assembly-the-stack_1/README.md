## The values 5, 34, 32, 12 are pushed to a data structure and when popped they return 12, 32, 34, 5\. What data structure does this represent?

KSATs: K0203, K0222, K0691

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| queue | vector | tree | ***stack*** |


Feedback:

- A) Incorrect: a queue is a first in first out data structure used in operating systems
- B) Incorrect: a vector is not a data structure used at the operating system level
- C) Incorrect: a tree is an abstract data type that simulates a hierarchical tree structure
- D) Correct: a stack is a last in, first out, data structures used in operating systems

[**<- Back To README**](../../README.md)

