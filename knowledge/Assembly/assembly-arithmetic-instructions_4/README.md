```nasm
1 | move rax, 5
2 | xor rax, rax
3 | 
```

## In the above Assembly code, what does rax contain after all the operations?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| 5 | 10 | 1 | ***0*** |


Feedback:

- For XOR, if the bits from the operands are same \(both 0 or both 1\), the resultant bit is cleared to 0\. Because both values have the same bits the results are all zeros

[**<- Back To README**](../../README.md)

