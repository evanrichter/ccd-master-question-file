## what is the purpose of the x86/x86\-64 CALL instruction?

KSATs: K0201, K0691, K0776

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Assign a name to a value so that it can be used later\. | ***Run a procedure declared elsewhere in code\.*** | Copy all values pointed to by a source into a destination | Cancel all instructions after the call instruction |


Feedback:

- call is used to execute instructions declared elsewhere in code similar to a function in other languages\.

[**<- Back To README**](../../README.md)

