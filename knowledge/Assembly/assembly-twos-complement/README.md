## What is the common use for two's compliment

KSATs: K0211, K0691

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| Quickly detect if a number is even\. | Encrypting data to securely transmit across a network\. | Divide or multiply by powers of 2\. | ***Convert binary representation of a positive integer to its negative\.*** |


Feedback:

- Two's compliment is used to convert positive integers into negative in binary\.

[**<- Back To README**](../../README.md)

