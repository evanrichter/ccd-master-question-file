## Instructions like MOV or ADD is called a\(n\) \_\_\_\_\_\_\_\_\_\_ in Assembly\.

KSATs: K0206, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Command | Interrupt | ***Opcode*** | Instruction |


Feedback:

- Opcode is short for operational code and is the terminology used for commands like MOV and ADD\.

[**<- Back To README**](../../README.md)

