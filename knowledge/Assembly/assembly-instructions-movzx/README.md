## What best describes the x86/x86\-64 movzx instruction?

KSATs: K0201, K0691, K0767

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| Copy the contents from the second operand into the first operand\. | Move the contents from the second operand into the first operand and set the second operand to zero\. | Copy the contents from the second operand into the first operand and fill the low bits with zeros | ***Copy the contents from the second operand into the first operand and fill the high bits with zeros*** |


Feedback:

- movzx will copy the contents from the source to the destination and extends to a full word or dword

[**<- Back To README**](../../README.md)

