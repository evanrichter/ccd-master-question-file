## What is the smallest addressable unit in an X86 architecture?

KSATs: K0201, K0219, K0204, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| word | ***byte*** | bit | segment |


Feedback:

- A) Incorrect: a word is a two byte addressable unit
- B) Correct: a byte is the smallest addressable unit
- C) Incorrect: a bit is not individually addressable
- D) Incorrect: a segment is a general term of a large portion of memory and contains several bytes

[**<- Back To README**](../../README.md)

