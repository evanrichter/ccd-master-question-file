```nasm
1 | mov rax, 2
2 | shr rax, 2
3 | shl rax, 2
4 | 
```

## In the above Assembly code, what's contained in the rax register when finished?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| 2 | 1 | ***0*** | 4 |


Feedback:

- mov rax, 2 puts \.\.\.00000010 into rax, shr \(shift right\) 2, shifts bits 2 to right resulting in \.\.\.00000000 in rax, shl \(shift left\) 1 shifts all bits one to left but has no affect because register is all zeros

[**<- Back To README**](../../README.md)

