## What is the smallest unit of data a computer can offer?

KSATs: K0219, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| binary | byte | ***bit*** | segment |


Feedback:

- A) Incorrect: binary is not a unit of data, but a term for how data is represented
- B) Incorrect: a byte consist of eight \(8\) bits
- C) Correct: a bit is the smallest unit of data in current computers
- D) Incorrect: a segment is a general term for a large portion of memory

[**<- Back To README**](../../README.md)

