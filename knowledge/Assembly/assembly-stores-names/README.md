## The assembler stores all the names and their corresponding values in the \_\_\_\_\_\.

KSATs: K0225, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Special Purpose Register | ***Symbol Table*** | Value Map Set | Stack |


Feedback:

- The symbol table contains information to locate and relocate symbolic definitions and references\. The assembler creates the symbol table section for the object file\. It makes an entry in the symbol table for each symbol that is defined or referenced in the input file and is needed during linking\.

[**<- Back To README**](../../README.md)

