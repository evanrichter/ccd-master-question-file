## Which register in Assembly is the default register used for a counter?

KSATs: K0213, K0202, K0204, K0233, K0528, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| rax | ***rcx*** | rdx | rdi |


Feedback:

- A) Incorrect: rax is where values are returned from functions
- B) Correct: rcx is the default counter register
- C) Incorrect: rdx can be freely used but is not the default counter
- D) Incorrect: rdi is used in 64\-bit Linux as function argument \#1 and in 64\-bit Windows it's preserved

[**<- Back To README**](../../README.md)

