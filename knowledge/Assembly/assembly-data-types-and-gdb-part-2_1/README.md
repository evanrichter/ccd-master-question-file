## In Assembly, which of the following is a valid "high" 8\-bit sub register?

KSATs: K0216, K0217, K0204, K0691

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***ah*** | al | ax | eax |


Feedback:

- A) Correct: the 'h' in ah indicates a high bit sub register and is 8 bits
- B) Incorrect: the 'l' in al indicates a low bit sub register and is 8 bits
- C) Incorrect: ax is a 16\-bit sub register
- D) Incorrect: eax is a 32\-bit sub register

[**<- Back To README**](../../README.md)

