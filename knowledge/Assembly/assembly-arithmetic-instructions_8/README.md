## Which is true about the x86/x86\-64 DIV instruction?

KSATs: K0201, K0691, K0778

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| The dividend register is the first operand and the divisor register is the second operand\. | ***The dividend is assumed to be in the RAX register\.*** | The first operand is the register to store the quotient\. | The FDIV flag is used to set the division mode between integer and floating point division\. |


Feedback:

- div assumes the dividend to be in the RAX register and the sole operand to be the divisor\. The quotient is placed in the RAX register with the remainder in the RDX register\.

[**<- Back To README**](../../README.md)

