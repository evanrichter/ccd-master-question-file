## This flag is set if the result of an add would have set bit 33 \(in x86\), or bit 65 \(in x86\_64\)\.

KSATs: K0201, K0213, K0214, K0202, K0238, K0528, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Zero Flag \(ZF\) | ***Carry Flag \(CF\)*** | Sign Flag \(SF\) | Bit Flag \(BF\) |


Feedback:

- A) Incorrect: this flag gets set if an arithmetic result is zero
- B) Correct: this flag is set if the result of an add would have set bit 33 \(in x86\), or bit 65 \(in x86\_64\)
- C) Incorrect: this flag is set to indicate the result of an operation is negative
- D) Incorrect: there is no Bit Flag

[**<- Back To README**](../../README.md)

