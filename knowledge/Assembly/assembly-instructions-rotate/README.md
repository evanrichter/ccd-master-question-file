```nasm
1 |mov al, 01110101b
2 |ror al, 3
3 |rol al, 4
```

## What is the value in register al after the above code runs?

KSATs: K0201, K0691, K0767, K0790

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| 11100000 | ***11101010*** | 01010111 | 11010101 |


Feedback:

- The ror instruction rotates the bits right with lost bits used to pad the front\. rol rotates to the left with the lost bits used to pad the lowest bits\.

[**<- Back To README**](../../README.md)

