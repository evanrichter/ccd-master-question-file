```nasm
1 | movzx rax, cl
2 | 
```

## In the above Assembly code, what's contained in the leftmost byte of the rax register?

KSATs: K0213, K0215, K0216, K0202, K0217, K0206, K0207, K0204, K0233, K0528, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| a copy of the cl contents | residual data | ***all zeroes*** | address of cl |


Feedback:

- A) Incorrect: Moving a smaller register \(c1\) to a larger register \(rax\) will result in the cl contents being placed in the rightmost bits
- B) Incorrect: The movzs puts all zeros in unfilled bits therefore there will be no residual data
- C) Correct: The movzx puts zeros in all unfilled bits from a mov command, because cl only has 8 bits, the leftmost 56 bits \(7 bytes\) are filled with zeros
- D) Incorrect: this mov command moves contents of a register not an address

[**<- Back To README**](../../README.md)

