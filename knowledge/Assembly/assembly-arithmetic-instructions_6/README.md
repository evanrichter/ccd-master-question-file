## The primary opcode to increment a number in a register by 1 is:

KSATs: 

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| \+\+ | ***inc*** | dec | mov |


Feedback:

- A) Incorrect: \+\+ is not a valid assembly opcode
- B) Correct: inc is the assembly opcode to increment a number by 1
- C) Incorrect: dec is the assembly opcode to decrement a number by 1
- D) Incorrect: mov puts a value \(replaces current value\) in a register

[**<- Back To README**](../../README.md)

