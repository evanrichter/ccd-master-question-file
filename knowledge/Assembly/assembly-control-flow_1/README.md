## Which of the following commands calls the kernel in Assembly programming?

KSATs: K0206, K0207, K0243, K0242, K0524, K0526, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| mov 0x80 | ***int 0x80*** | call 0x80 | sys\_exit |


Feedback:

- A) Incorrect: the mov opcode is used to mov contents into a register
- B) Correct: the int opcode is used to call the kernel
- C) Incorrect: call is not a valid opcode
- D) Incorrect: sys\_exit is not a valid opcode

[**<- Back To README**](../../README.md)

