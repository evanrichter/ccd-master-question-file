## In Assembly, what instruction is used to check if two register's contents are equal?

KSATs: K0206, K0207, K0691

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***cmp*** | eq | == | shr |


Feedback:

- A) Correct: cmp is the correct opcode used to compare two register's contents
- B) Incorrect: eq is not a valid assembly opcode
- C) Incorrect: == is not a valid assembly opcode
- D) Incorrect: shr is the opcode used to right\-shift a register's bits

[**<- Back To README**](../../README.md)

