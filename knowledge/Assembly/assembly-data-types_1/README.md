## Which of the following is a valid example of declaring and instantiating a variable in Assembly programming?

KSATs: K0206, K0219, K0204, K0691

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***age DW 12345*** | age int 12345 | age  DW=12345 | age 12345 |


Feedback:

- A) Correct: variables are declared and instantiated with a name, type, and value each separated by a space\. DW \(define word\) is valid data type in assembly
- B) Incorrect: variables are declared and instantiated with a name, type, and value each separated by a space\. int is valid data type in assembly
- C) Incorrect: the = operator is not valid syntax for initializing a variable in assembly
- D) Incorrect: variables are declared and instantiated with a name, type, and value each separated by a space\. no data type is provide here

[**<- Back To README**](../../README.md)

