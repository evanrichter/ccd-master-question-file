## In Assembly, what instruction places a value on the stack?

KSATs: K0203, K0206, K0229, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| add | insert | ***push*** | stack |


Feedback:

- A) Incorrect: The add opcode is used to add to and existing register's content
- B) Incorrect: insert is not a valid assembly opcode
- C) Correct: push is the correct opcode to use to push a value onto the stack
- D) Incorrect: stack is not a valid assembly opcode

[**<- Back To README**](../../README.md)

