```nasm
1 | mov rax, 1
2 | shl rax, 1
3 | shl rax, 3
4 | 
```

## In the above Assembly code, what's contained in the rax register when finished?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***16*** | 8 | 1 | 2 |


Feedback:

- Correct: mov rax, 1 establishes 00000001 in the register, shl \(shift left\) 1 modifies to: 00000010 \(now 2\), shl 3 modifies to: 00010000 \(now 16\)

[**<- Back To README**](../../README.md)

