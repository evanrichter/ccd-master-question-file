## The text section of an Assembly program must begin with what declaration?

KSATs: K0233, K0225, K0528, K0691

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| \_begin | start | section | ***global \_start*** |


Feedback:

- Correct: The text must begin with the declaration global \_start\. This tells the kernel where the program execution begins

[**<- Back To README**](../../README.md)

