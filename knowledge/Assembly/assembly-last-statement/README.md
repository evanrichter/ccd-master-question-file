## The last statement in an Assembly source program should be:

KSATs: K0233, K0225, K0691

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| stop | ***end*** | return | exit |


Feedback:

- The end statement in Assembly is the last line of the program or function\.

[**<- Back To README**](../../README.md)

