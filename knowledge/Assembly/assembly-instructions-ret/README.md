## What best describes the x86/x86\-64 ret instruction?

KSATs: K0691, K0777

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| returns a value to the calling function\. | returns the stack pointer to zero\. | returns flow to the top of a loop\. | ***resumes execution flow at the address popped off on the stack\.*** |


Feedback:

- The ret instruction transfers control to the return address located on the stack\.

[**<- Back To README**](../../README.md)

