## Which assembly instruction's primary function is to advance the instruction pointer to the next instruction and can be used for padding/alignment and timing reasons?

KSATs: K0206, K0225, K0691

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***nop*** | xor | cmp | pad |


Feedback:

- A) Correct: the nop \(no operation\) opcode is used to advance the instruction pointer to the next instruction
- B) Incorrect: the xor opcode executes and exclusive or operation on two registers
- C) Incorrect: the cmp opcode compares two registers for equality
- D) Incorrect: pad is not a valid assembly opcode

[**<- Back To README**](../../README.md)

