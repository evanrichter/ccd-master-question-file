## What best describes the x86/x86\-64 SUB instruction?

KSATs: K0201, K0691, K0778

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Replace all occurrences of the second operand with the first operand\. | Moves the instruction pointer to location pointed by the first argument\. | ***Subtracts the second operand from the first\.*** | Copy the contents from the second operand into the first operand and fill the high bits with zeros\. |


Feedback:

- Sub is used to subtract the second operand from the first\.

[**<- Back To README**](../../README.md)

