## What's the purpose of the assembler?

KSATs: K0209, K0691

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| places all function calls in a register\. | organizes the registers to optimize execution\. | dynamically allocates all needed memory\. | ***converts assembly language into machine code\.*** |


Feedback:

- An assembler is a program that converts assembly language into machine code\. It takes the basic commands and operations from assembly code and converts them into binary code that can be recognized by a specific type of processor\.

[**<- Back To README**](../../README.md)

