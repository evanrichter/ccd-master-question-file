```nasm
1 |mov eax, 0xA    ;0xA is equivalent to 1010 in binary
2 |shr eax, 2
3 |shl eax, 1
```

## What is the value in register eax after the above code runs?

KSATs: K0201, K0691, K0767, K0784

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| 1010 | 10 | ***100*** | 101 |


Feedback:

- The shr instruction shifts the bits right by 2 bits resulting in 10\. The shl instruction then shifts left the bits by one resulting in 100

[**<- Back To README**](../../README.md)

