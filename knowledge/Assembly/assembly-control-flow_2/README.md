```nasm
1  | .label1:
2  |     xor rax, rax
3  |     inc rax
4  |     mov rcx, rax
5  |     jmp .label2
6  |     mov rsp, rax
7  | .label2:
8  |     shl rcx, 3
9  |     xchg rcx, rax
10 |     ret
11 | 
```

## In the above Assembly code, which of the following is true?

KSATs: K0202, K0206, K0207, K0204, K0240, K0231, K0528, K0691

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***The line mov rsp, rax is never executed*** | label1 and label2 are incorrectly defined | shl cannot be executed on the rcx register | jmp can only be used for label1 |


Feedback:

- A) Correct: this line is never executed because it is preceded by a jmp command with will always jump to the label2 when executed
- B) Incorrect: both these labels are correctly defined
- C) Incorrect: shl can be executed on the rcx register
- D) Incorrect: the jmp opcode can be used for any label

[**<- Back To README**](../../README.md)

