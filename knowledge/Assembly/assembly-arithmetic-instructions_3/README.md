```nasm
1 | mov rdx, 0
2 | mov rax, 10
3 | mov rcx, 2
4 | div rcx
5 | 
```

## In the above Assembly code, what does rax contain after all the operations?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| 10 | 0 | ***5*** | 1 |


Feedback:

- div takes a single argument \(value in rcx\), and divides the value stored in the dividend register \(rax\) by it\.

[**<- Back To README**](../../README.md)

