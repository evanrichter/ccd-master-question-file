## In Assembly, for a 64\-bit register rdx, its 32\-bit sub register is \_\_\_\_\.

KSATs: K0213, K0214, K0215, K0202, K0217, K0204, K0528, K0691

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| eax | dx | ***edx*** | rdl |


Feedback:

- A) Incorrect: eax is the 32\-bit sub register for rax
- B) Incorrect: dx is the 16\-bit sub register for rdx
- C) Correct: edx is the 32\-bit sub register for rdx
- D) Incorrect: rdl is not a valid register name

[**<- Back To README**](../../README.md)

