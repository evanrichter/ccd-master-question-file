## What does the last node of a multi\-node circularly linked list point to?

KSATs: K0058, K0700

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***The first node*** | NULL | Itself | The previous node |


Feedback:

- Circularly linked nodes use nodes similar to regular linked lists except the last node in a circularly linked list points to the first node \- no node points to NULL

[**<- Back To README**](../../README.md)

