## Which of the following is a proper behavior for a stack?

KSATs: K0061, K0700, K0751

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Items can be removed anywhere in the stack if it's implemented with a linked\-list\. | ***Only the last item added can be removed from the stack\.*** | A stack is a last in last out data structure\. | Both B & C are correct\. |


Feedback:

- Only the last item added can be removed via a pop function for a stack\.  A stack is last in first out data structure\.

[**<- Back To README**](../../README.md)

