## Which of the following is a disadvantage of using a linked\-list over an array in C for the same amount of data?

KSATs: K0060, K0700

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| linked\-lists use more memory | linked\-lists cannot be randomly accessed | linked\-lists nodes do not use contiguous memory | ***all the above*** |


Feedback:

- All are disadvantages of using linked lists over arrays\.

[**<- Back To README**](../../README.md)

