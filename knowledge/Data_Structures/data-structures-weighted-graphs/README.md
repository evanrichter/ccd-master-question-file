## In a Weighted Graph, the weight is applied to each of the graph's \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

KSATs: K0059, K0700

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| nodes | vertices | levels | ***edges*** |


Feedback:

- A weighted graph refers to a simple graph that has weighted edges\. These weighted edges can be used to compute the shortest path\.

[**<- Back To README**](../../README.md)

