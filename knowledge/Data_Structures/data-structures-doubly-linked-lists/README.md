```c
1  |  struct Node {
2  |    int data;
3  |    struct Node *next;
4  |    struct Node *prev;
5  |  };
6 | 
```

## The above C struct definition is ideal for what type of Data Structure?

KSATs: K0057, K0700

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Linked List | ***Doubly Linked List*** | Binary Tree | Graph |


Feedback:

- A) Incorrect: Linked list nodes will only have a single pointer to the next node\.
- B) Correct: Doubly linked lists will have a pointer that points to the previous node or NULL, and to the next node or NULL
- C) Incorrect: Although Binary Tree nodes contain two pointers, these typically are labeled left and right respectively
- D) Incorrect: Each node in a graph can point to more than two other graph nodes\. Therefore there must be a way to track an unknown number of nodes such as with an array\.

[**<- Back To README**](../../README.md)

