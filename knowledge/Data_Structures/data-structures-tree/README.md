```c
1 | class Node:
 2 |     def __init__(self, data):
 3 |         self.data = data
 4 |         self.children = []

```

## The above class is ideal for what type of data structure?

KSATs: K0700, K0753

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| linked\-list\. | doubly\-linked list\. | ***general tree*** | binary tree |


Feedback:

- A) Incorrect: A linked list has a node typically consisting of data and a next ref to the next node or to None\.
- B) Incorrect: A doubly\-linked list has a node typically consisting of data and a next reference the next node or to None and a prev reference to the previous node or None\.
- C) Correct: A general tree is a data structure where every child has only one parent, but one parent can have many children\.
- D) Incorrect: although this could be used for a binary tree, binary tree are limited parents have two children, therefore the node would consist of a left reference the left node/child or to None and a right reference to the right node/child or None\.

[**<- Back To README**](../../README.md)

