## Which of the following is a proper behavior for a queue?

KSATs: K0061, K0700, K0752

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Items can be added to the front of the queue if it's implemented with a linked\-list\. | ***Items are removed from the front of the queue\.*** | A queue is a Last In, First Out data structure\. | Both B & C are correct\. |


Feedback:

- Only the first item in the queue \(front\) can be removed via a dequeue function for a queue\.  A queue is first in first out data structure\.

[**<- Back To README**](../../README.md)

