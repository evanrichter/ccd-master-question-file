## Which of the following supports good modular design practices?

KSATs: K0002

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Creating meaningful variable names\. | Creating functions with two parameters or fewer\. | ***Creating reusable functions that provide a unique service\.*** | If there are more than five sequential statements in a program, a function should be created for it\. |


Feedback:

- All functions should provide a unique service, functions that are reusable support modular design principles\.

[**<- Back To README**](../../README.md)

