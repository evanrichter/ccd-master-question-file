## Which of these data structures has a constant search time for the worst case scenario?

KSATs: K0052, K0104, K0110, K0689

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Array | Linked list | ***Hash tables*** | Stack |


Feedback:

- A) Incorrect: While arrays have constant access time, they must still be iterated over to find elements\.
- B) Incorrect: Linked lists must iterate through their list to find or access an element\.
- C) Correct: Hash tables use hashing functions to find elements without iterating\.
- D) Incorrect: A stack must pop each of element off to find the one after it\.

[**<- Back To README**](../../README.md)

