```c
1 | char *text = "This is a test"; 
2 | text += 2;
3 | 
```

## Given the above code, what is the dereferenced value of text?

KSATs: K0096, K0103, K0689

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| is is a test | ***i*** | an address | This is a test |


Feedback:

- dereferencing a char \* will give you the value of the character the pointer is assigned to, the text pointer originates a the beginning of the string, then is move two characters to the i in 'This'

[**<- Back To README**](../../README.md)

