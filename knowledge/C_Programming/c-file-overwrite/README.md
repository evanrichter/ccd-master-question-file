## Which c function call opens the file 'example\.txt' in write mode and overwrites it if it exists?

KSATs: K0689, K0747, K0748

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| fopen\("example\.txt", "a\+"\) | open\("example\.txt"\) | ***fopen\("example\.txt","w"\)*** | printf\("example\.txt"\) |


Feedback:

- fopen will open a file in read mode by default\. Passing w as the second argument will open in write mode at the start of the file, allowing writes to overwrite existing data\.

[**<- Back To README**](../../README.md)

