```c
1 | float *payIncrease(float factor)
2 | {
3 |     float salary[] = {24.5, 33.1, 17.2, 18.4};
4 |     for(int i = 0; i < 4; i++)
5 |         salary[i] *= factor;
6 |     return salary;
7 | }
```

## Given the above C function, what logical problem exists?

KSATs: K0689, K0740

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| C does not support a return type of a pointer | ***The function returns the address of a local array*** | The for loop has a one\-off error | The array should have a declared size to prevent overflow |


Feedback:

- the function returns the address of salary which is a local array\. Any caller to this function will receive an address to an array that no longer exists\.

[**<- Back To README**](../../README.md)

