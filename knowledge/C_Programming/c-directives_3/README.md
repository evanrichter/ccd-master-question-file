## Which of the following is executed by the preprocessor in C?

KSATs: K0105, K0689

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***\#include<stdio\.h>*** | return 0 | void main \(int argc , char \*\* argv\) | None of above |


Feedback:

- preprocessor instructions are preceded with a \#\. Examples include \#define, \#include, \#pragma etc\.

[**<- Back To README**](../../README.md)

