```c
1  |  int ages[] = {23,34,31,54,61,14,8};
2  |     int *aPtr = ages, *end = &ages[6];
3  |     int val = 0;
4  |     while(aPtr < end)
5  |     {
6  |         val += *aPtr;
7  |         aPtr += 2;
8  |     } 
9  |  }
10 | 
```

## What is the value of val after the above C code snippet executes?

KSATs: K0689, K0761

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***115*** | 217 | 231 | 97 |


Feedback:

- pointers and pointer arithmetic is used to go through the array\. The pointer starts at the beginning of the array, gets dereferenced to get its value, and is added to val\.  The pointer is increase by 2 moving it to two indexes further in the array\. this is repeated until the end of the array is met when the address of aPtr becomes a higher value than the address of end\.  Essentially, every other value in the array is added up\.

[**<- Back To README**](../../README.md)

