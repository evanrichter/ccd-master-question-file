## In C programming, what command releases memory that was created on the heap?

KSATs: K0122, K0121, K0689, K0758

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| memfree | ***free*** | dealloc | delete |


Feedback:

- The free function is what is used to release any memory allocated with malloc\.

[**<- Back To README**](../../README.md)

