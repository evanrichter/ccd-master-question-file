## Which of the following operator, known as a dereference, can be used to access value at address stored in a pointer variable?

KSATs: K0103, K0689

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| & | \# | ***\**** | \-> |


Feedback:

- The asterisk \* is the dereference operator use to access the contents a pointer refers to\.

[**<- Back To README**](../../README.md)

