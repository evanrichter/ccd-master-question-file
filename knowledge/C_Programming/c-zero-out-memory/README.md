## What is the purpose the C library function calloc?

KSATs: K0689, K0823

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***allocates memory and sets allocated memory to zero*** | allocates memory and sets allocated memory to NULL | allocates memory but does not modify the memory | captures allocated memory created by the malloc function |


Feedback:

- The C library function void \*calloc\(size\_t nitems, size\_t size\) allocates the requested memory and returns a pointer to it\. The difference in malloc and calloc is that malloc does not set the memory to zero where as calloc sets allocated memory to zero\.

[**<- Back To README**](../../README.md)

