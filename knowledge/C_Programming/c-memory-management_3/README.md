## What C command is use to allocate memory dynamically?

KSATs: K0122, K0689, K0735, K0757

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***malloc*** | alloc | new | memcpy |


Feedback:

- malloc is the command used to allocate memory dynamically \(at run time\)

[**<- Back To README**](../../README.md)

