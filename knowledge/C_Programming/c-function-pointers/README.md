```c
1 | int getTotal(int a, int b) 
 2 | { 
 3 |     return a + b; 
 4 | } 
 5 |   
 6 | int main() 
 7 | { 
 8 |     int (*tot)(int, int) = &getTotal; 
 9 |     int x = (*tot)(10, 20);
 10 |     return 0; 
 11 | }
```

## What is occuring on line 8 in the above C code?

KSATs: K0689, K0760

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| The getTotal function is being invoked\. | The getTotal function is being redefined to return a int pointer\. | ***A function pointer is being created for getTotal\.*** | getTotal is being allocated in the heap\. |


Feedback:

- A function  pointer is created by assigning the address of the function name \(&getTotal\) to a pointer that also has the signature of the function \(quantity, type, and order of parameters\)\.

[**<- Back To README**](../../README.md)

