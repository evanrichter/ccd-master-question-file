```c
1 | enum week{Mon, Tue, Wed, Thur, Fri, Sat, Sun}; 
2 | printf("%d", Fri); 
```

## What is printed in the C program snippet above?

KSATs: K0126, K0689

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Fri | ***4*** | 0 | 5 |


Feedback:

- Enumerated types have an implicit, default integer value starting at 0 for the first item and incrementing by 1 for each following item

[**<- Back To README**](../../README.md)

