```c
1 | int *x = malloc(sizeof(int));
2 | 
```

## In the above statement, where is the memory pointed to by x allocated?

KSATs: K0103, K0119, K0122, K0121, K0689, K0735, K0757

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Stack | Registry | ***Heap*** | Cache |


Feedback:

- Memory allocated at run time is placed on the heap and is done using the malloc\(\) function\. Compile time memory is allocated on the stack

[**<- Back To README**](../../README.md)

