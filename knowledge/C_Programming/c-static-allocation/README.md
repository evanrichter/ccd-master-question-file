```c
1 | int x = 2; 
2 | 
```

## When is the memory for x being allocated in the above C snippet?

KSATs: K0122, K0689

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Dynamically, at run time | ***Statically, at compile time*** | Virtually, when requested | Preliminary, just before accessed |


Feedback:

- All variables are allocated memory on the stack

[**<- Back To README**](../../README.md)

