```c
 ~1011 
```

## What is the result of the above C code snippet?

KSATs: K0111, K0689

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| 1011 | ***0100*** | 0000 | 1111 |


Feedback:

- Binary One's Complement Operator is unary and has the effect of 'flipping' each bit in the operand\.

[**<- Back To README**](../../README.md)

