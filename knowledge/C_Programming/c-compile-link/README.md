## During a build, at what stage would the build fail if a function does not exist that is being called?

KSATs: K0140, K0689

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| preprocess | compile | ***link*** | deploy |


Feedback:

- The linking stage is when verification of functions exist that are being invoked\. main\(\) of course is the primary function invoked in a program\.

[**<- Back To README**](../../README.md)

