```c
1 | double addValues(double, double);
2 | 
```

## The above C code is an example of a:

KSATs: K0009, K0689

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| function | method | ***prototype*** | procedure |


Feedback:

- a function prototype provides a definition of a functions signature\. The signature of a function includes its name, return type, the number, order, and type of parameters\. A prototype does not contain any executable code\.

[**<- Back To README**](../../README.md)

