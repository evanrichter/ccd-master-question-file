```c
 1011 | 0010 
```

## What is the result of the above C code snippet?

KSATs: K0111, K0689

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***1011*** | 0011 | 0010 | 1001 |


Feedback:

- Binary OR Operator retains a bit if it exists in either operand in that position\.

[**<- Back To README**](../../README.md)

