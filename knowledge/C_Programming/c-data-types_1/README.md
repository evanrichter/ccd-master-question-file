## Which of the following is a valid ASCII char declaration in C?

KSATs: K0096, K0128, K0689

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***char x = '\\n'*** | char x = "A"; | char x = \-3; | None of the above |


Feedback:

- A) Correct: \\n is a special character included in a series of escape sequences\. This character represents a newline
- B) Incorrect: "A" is in double\-quotes which is a string or char array literal
- C) Incorrect: \-3 is not a valid integer value for an ASCII char

[**<- Back To README**](../../README.md)

