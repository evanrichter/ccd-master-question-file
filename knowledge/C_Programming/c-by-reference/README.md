```c
1 | void processData(int arr[], int size)
2 | {
 3 |   arr[0]++;
4 |   size++;
5 | }
6 | int main()
7 | {
8 |    int x[] = {2,4,6,8};
9 |    int y = 4;
10 |    processData(x, y);
11 | }
```

## Given the above C code snippet, what are the values of x\[0\] and y after execution?

KSATs: K0689, K0739

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| x\[0\] is 2 and y is 4 | x\[0\] is 3 and y is 5 | ***x\[0\] is 3 and y is 4*** | x\[0\] is 0 and y is 4 |


Feedback:

- When an array name is passed to a function it is passed by reference\. When a scalar variable, like an int variable, is passed to a function, it is passed by value\. Because x is passed by reference, the variable arr inside the function is an alias to x, so any changes made via arr affect x\.  This is not the same case for pass by value variables like y as a copy of the variable is made in the function and any changes to that variable do not effect the passed variable\.

[**<- Back To README**](../../README.md)

