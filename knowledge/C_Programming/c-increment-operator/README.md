```c
   1 | int x = 5;
 2 |     x++;
 3 |     printf("%d",x++);
 
```

## What is printed in the above snippet?

KSATs: K0689, K0764

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| 5 | ***6*** | 7 | d5 |


Feedback:

- The increment operator has a pre and post function\.  The \+\+ after the variable is executed after the statement is executed\.  So, the print occurs first, then x is incremented to 7 after the print\.

[**<- Back To README**](../../README.md)

