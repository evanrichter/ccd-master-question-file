```c
1 | struct person {
2 |   char *name;
3 |   int age;
4 | } artist;
5 | 
```

## Given the above C code snippet, how would you initialize name in the struct person?

KSATs: K0103, K0124, K0128, K0689

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| name\.artist = "Picasso"; | person\.name = "Picasso"; | artist\->name = "Picasso"; | ***artist\.name = "Picasso";*** |


Feedback:

- Line 4 declares a variable artist of type struct person simultaneously when the struct is defined\. Accessing individual elements within a struct variable is done by using the \. operator: artist\.name, artist\.age etc\.  If artist were declared as a pointer, then accessing each element would use the \-> operator such as:  artist\->name, artist\->age\.

[**<- Back To README**](../../README.md)

