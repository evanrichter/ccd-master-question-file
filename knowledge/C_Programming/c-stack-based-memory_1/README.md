```c
1 | int *ptr1 = malloc(sizeof(int));
2 | 
```

## Where is ptr1 stored?

KSATs: K0103, K0120, K0122, K0121, K0689

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Cache | ***The stack*** | The heap | None of the above |


Feedback:

- While the memory allocated by the expression "malloc\(sizeof\(int\)\)" is allocated on the heap, the pointer "ptr1" that holds that address is allocated on the stack at compile time

[**<- Back To README**](../../README.md)

