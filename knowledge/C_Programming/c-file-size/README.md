```c
1 | FILE* fp = fopen("file.txt", "r");
2 | fseek(fp, 0L, SEEK_END);
3 | int res = ftell(fp);
```

## What does the variable res contain after the above snippet executes?

KSATs: K0689, K0749

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| The last character in the file | ***The file size in bytes*** | The number of lines in the file | The address of the last character in the file |


Feedback:

- Using fseek to move the file pointer to the end of the file\. ftell returns the value of the position indicator, so at position 0 you're at the beginning of the file\. At position n you're at the nth byte of the file\. So seeking to the end of file then using ftell will return the size of the file in bytes\.

[**<- Back To README**](../../README.md)

