## Which of the following is true about the 'main' function in C programming?

KSATs: K0093, K0689

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| There can be multiple 'main' functions in a program\. | The 'main' function does not have a return type\. | ***All execution begins and ends in the 'main' function\.*** | 'main' is an optional function to have in a program\. |


Feedback:

- A) False: A C program can only have one main function\.
- B) False: The main function needs a return type even if it is void\.
- C) True: The main function is where all execution begins and ends in a C program
- D) False: All C programs must have a main function\.

[**<- Back To README**](../../README.md)

