## Given the main function defined to receive command line arguments:  int main\( int argc, char \*argv\[\], what will be in argv\[1\] if the following command is execute: \./myProgram blue green red?

KSATs: K0765

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| 0 | ***blue*** | green | red |


Feedback:

- argv\[0\] is reserved to contain the name of the program\. The first argument passed will be in the \[1\] index\.

[**<- Back To README**](../../README.md)

