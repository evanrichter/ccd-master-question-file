## When using threads, what does the pthread\_self\(\) function do?

KSATs: K0185, K0689

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| it returns the address of the current thread | it makes a copy of the current thread | ***it returns the id of the current thread*** | it creates a new thread instance |


Feedback:

- The pthread\_self\(\) function is used to get the ID of the current thread\.

[**<- Back To README**](../../README.md)

