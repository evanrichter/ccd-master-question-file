## Which function would you use to point to the 10th byte in a file in C?

KSATs: K0689, K0750

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| seek\(\) | fgetc\(\) | move\(\) | ***fseek\(\)*** |


Feedback:

- the function fseek is used to move the position of the stream in a file to the location specified\.

[**<- Back To README**](../../README.md)

