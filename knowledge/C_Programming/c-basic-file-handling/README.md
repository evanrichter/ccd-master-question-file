## Which of the following is true when reading and writing files in C?

KSATs: K0689, K0746, K0747

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| File operations are all functions called with a File object using the dot \(\.\) operator | You can only open in either read or write mode, not both | ***You have to explicitly close files that are opened*** | You can only write one character at a time |


Feedback:

- A) Incorrect\. Objects do not exist in C\. The file IO operations in stdio\.h are functions that you pass file names or file descriptors to for them to affect\.
- B) Incorrect\. You can pass r\+ or a\+ to open a file in read and write mode\.
- C) Correct\. File descriptors do not close automatically and unclosed files can use up the file descriptor table\.
- D) Incorrect\. You can use fputs\(\) to write character arrays to files\.

[**<- Back To README**](../../README.md)

