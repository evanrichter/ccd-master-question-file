```c
1 | int x = 2; 
2 | 
```

## Where is the memory for x being allocated in the above C snippet?

KSATs: K0120, K0689, K0735

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| registers | heap | disk | ***stack*** |


Feedback:

- All variables are allocated memory on the stack

[**<- Back To README**](../../README.md)

