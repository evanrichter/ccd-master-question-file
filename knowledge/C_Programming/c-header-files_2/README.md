## What should not be included in C header files?

KSATs: K0101, K0689

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| Function prototype | Include guards | Type and struct definitions | ***Function implementation*** |


Feedback:

- C header files should avoid having implementation code\.

[**<- Back To README**](../../README.md)

