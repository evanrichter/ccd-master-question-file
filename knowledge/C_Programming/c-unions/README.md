```c
1 | union Data {
2 |    int i;
3 |    float f;
4 |    char str[20];
5 | };
```

## Given the above C union declaration, which is the proper way to declare a Data variable?

KSATs: K0125, K0689

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***union Data mydata;*** | Data mydata; | Data\.union\(mydata\); | Data union mydata; |


Feedback:

- to create a union you must use the keyword union, followed by the type declared 'Data', followed by the name of the variable you want to create

[**<- Back To README**](../../README.md)

