## Which of the following is true about a return statement in the body of a function?

KSATs: K0689, K0738

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| All functions must have a return statement\. | void functions must return void or null\. | ***void functions don't need a return statement*** | the item returned cannot be a memory address |


Feedback:

- void functions do not return a value therefore do not require a return statement\. You can optionally provide a return statement with no value\.

[**<- Back To README**](../../README.md)

