## Which of the following is a preprocessor instruction?

KSATs: K0689, K0762

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| \.import | ***\#include*** | &malloc | \- link |


Feedback:

- All preprocessor instructions begin with a \#\.

[**<- Back To README**](../../README.md)

