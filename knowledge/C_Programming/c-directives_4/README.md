## To utilize a header file like stdio\.h in C programming, you use the \_\_\_\_\_\_\_\_\_\_ preprocessor directive\.

KSATs: K0105, K0689

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| \#define | \#using | \#import | ***\#include*** |


Feedback:

- A) Incorrect: although \#define is a valid preprocessor directive, it is not used to utilize header files\. this is used to define named constants
- B) Incorrect: \#using is not a valid preprocessor directive
- C) Incorrect: \#import is not a valid preprocessor directive in C
- D) Correct: \#include allows you to utilize standard C library headers as well as locally defined header files

[**<- Back To README**](../../README.md)

