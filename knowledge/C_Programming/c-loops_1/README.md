## Which of the following are valid loop keywords in C?

KSATs: K0102, K0107, K0689

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| for | do while | repeat | ***both a and b*** |


Feedback:

- There are 3 types of loops in C programming:  while, do while, and for

[**<- Back To README**](../../README.md)

