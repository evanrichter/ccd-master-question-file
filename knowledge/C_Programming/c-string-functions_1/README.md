## Which string function copies bytes from one char \* array to another char \* array and also limits number of bytes copied?

KSATs: K0187

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| strcpy\. | strstr\. | ***strncpy\.*** | strncat\. |


Feedback:

- A) Incorrect: strcpy copies bytes from one char \* array to another char \* array but does not limit number of bytes copied
- B) Incorrect: strstr is used to search a string for a provided substring\.
- C) Correct: strncpy function copies bytes from one char \* array to another char \* array and also limits number of bytes copied\.
- D) Incorrect: strncat is used to concatenate one string with another and limits the number of characters to concatenate\.

[**<- Back To README**](../../README.md)

