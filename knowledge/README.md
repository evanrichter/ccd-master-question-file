# Master Question List

## ***Algorithms***

## In computer science, what term is used for the notation to classify algorithms according to how their running time or space requirements grow as the input size grows?

KSATs: K0132

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Capacity measure | Big O notation | Complexity rating | Weighted measure |

Click [here](Algorithms/algorithms-big-O/README.md) to see the answers.

```mermaid
graph TD
  A-->B
  A-->C
  B-->D
  B-->E
  C-->F
  C-->G

```

## In the above graph, using a breadth\-first traversal, which node would be searched next given the current search was on node B?

KSATs: K0047

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| C | D | E | A |

Click [here](Algorithms/algorithms-breadth-first/README.md) to see the answers.

```mermaid
graph TD
  A-->B
  A-->C
  B-->D
  B-->E
  C-->F
  C-->G

```

## Referring to the above depicted graph, using a depth\-first traversal, which node would be searched next given the current search was on node B?

KSATs: K0046

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| C | D | E | D or E depending on implementation |

Click [here](Algorithms/algorithms-depth-first/README.md) to see the answers.

## Pertaining to Time Complexity, what is the average case Big\(O\) for inserting, searching, or deleting from a hash table?

KSATs: K0139

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |

Click [here](Algorithms/algorithms-hash-table/README.md) to see the answers.

## Pertaining to Time Complexity, what is the average case Big\(O\) for the heap sort algorithm for an array containing n elements?

KSATs: K0136

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |

Click [here](Algorithms/algorithms-heap-sort/README.md) to see the answers.

## Pertaining to Time Complexity, what is the average case Big\(O\) for the insertion sort algorithm for an array containing n elements?

KSATs: K0133

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |

Click [here](Algorithms/algorithms-insertion-sort/README.md) to see the answers.

## Pertaining to Time Complexity, what is the average case Big\(O\) for the merge sort algorithm for an array containing n elements?

KSATs: K0135

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |

Click [here](Algorithms/algorithms-merge-sort/README.md) to see the answers.

## Pertaining to Time Complexity, what is the average case Big\(O\) for the quick sort algorithm for an array containing n elements?

KSATs: K0137

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |

Click [here](Algorithms/algorithms-quick-sort/README.md) to see the answers.

## Pertaining to Time Complexity, what is the average case Big\(O\) for the selection sort algorithm for an array containing n elements?

KSATs: K0134

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| O\(1\) | O\(n\) | O\(n\*log\(n\)\) | O\(n\*n\) |

Click [here](Algorithms/algorithms-selection-sort/README.md) to see the answers.

## ***Assembly***

## In Assembly, assume the rsi register contains the address of an array\.  How do you access the data in the first element of the array?

KSATs: K0203, K0202, K0221, K0204, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \*rsi | rsi\[0\] | \[rsi\] | rsi\->\[0\] |

Click [here](Assembly/assembly-advanced-types-and-concepts_1/README.md) to see the answers.

```nasm
1 | mov eax, 12
2 | mov ebx, 4
3 | add eax, ebx
4 | inc ebx
5 | inc eax
6 | shr eax, 1
7 | 
```

## In the above Assembly code, what does eax contain after all the operations?

KSATs: K0201, K0214, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 17 | 16 | 8 | 18 |

Click [here](Assembly/assembly-arithmetic-instructions_1/README.md) to see the answers.

## Which register in Assembly is the default register used for a counter?

KSATs: K0213, K0202, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| rax | rcx | rdx | rdi |

Click [here](Assembly/assembly-arithmetic-instructions_2/README.md) to see the answers.

```nasm
1 | mov rdx, 0
2 | mov rax, 10
3 | mov rcx, 2
4 | div rcx
5 | 
```

## In the above Assembly code, what does rax contain after all the operations?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10 | 0 | 5 | 1 |

Click [here](Assembly/assembly-arithmetic-instructions_3/README.md) to see the answers.

```nasm
1 | move rax, 5
2 | xor rax, rax
3 | 
```

## In the above Assembly code, what does rax contain after all the operations?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 5 | 10 | 1 | 0 |

Click [here](Assembly/assembly-arithmetic-instructions_4/README.md) to see the answers.

## In Assembly, which of the following is the proper way to add 1 to the value stored in the rax register?

KSATs: K0213, K0202, K0207, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| mov rax, rax\+1 | rax\+\+ | add rax, 1 | add rax\+1 |

Click [here](Assembly/assembly-arithmetic-instructions_5/README.md) to see the answers.

## The primary opcode to increment a number in a register by 1 is:

KSATs: 

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \+\+ | inc | dec | mov |

Click [here](Assembly/assembly-arithmetic-instructions_6/README.md) to see the answers.

## What best describes the x86/x86\-64 SUB instruction?

KSATs: K0201, K0691, K0778

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Replace all occurrences of the second operand with the first operand\. | Moves the instruction pointer to location pointed by the first argument\. | Subtracts the second operand from the first\. | Copy the contents from the second operand into the first operand and fill the high bits with zeros\. |

Click [here](Assembly/assembly-arithmetic-instructions_7/README.md) to see the answers.

## Which is true about the x86/x86\-64 DIV instruction?

KSATs: K0201, K0691, K0778

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The dividend register is the first operand and the divisor register is the second operand\. | The dividend is assumed to be in the AX register\. | The first operand is the register to store the quotient\. | The FDIV flag is used to set the division mode between integer and floating point division\. |

Click [here](Assembly/assembly-arithmetic-instructions_8/README.md) to see the answers.

## What's the purpose of the assembler?

KSATs: K0209, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| places all function calls in a register\. | organizes the registers to optimize execution\. | dynamically allocates all needed memory\. | converts assembly language into machine code\. |

Click [here](Assembly/assembly-assembler/README.md) to see the answers.

```nasm
1 | movzx rax, cl
2 | 
```

## In the above Assembly code, what's contained in the leftmost byte of the rax register?

KSATs: K0213, K0215, K0216, K0202, K0217, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| a copy of the cl contents | residual data | all zeroes | address of cl |

Click [here](Assembly/assembly-basics-and-memory_1/README.md) to see the answers.

## In Assembly, what instruction places a value on the stack?

KSATs: K0203, K0206, K0229, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| add | insert | push | stack |

Click [here](Assembly/assembly-basics-and-memory_2/README.md) to see the answers.

## In Assembly, what instruction is used to check if two register's contents are equal?

KSATs: K0206, K0207, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| cmp | eq | == | shr |

Click [here](Assembly/assembly-basics-and-memory_3/README.md) to see the answers.

## Which of the following is a general purpose register?

KSATs: K0201, K0202, K0204, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ebp | rri | esp | rdx |

Click [here](Assembly/assembly-basics-and-memory_4/README.md) to see the answers.

## Which assembly instruction's primary function is to advance the instruction pointer to the next instruction and can be used for padding/alignment and timing reasons?

KSATs: K0206, K0225, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| nop | xor | cmp | pad |

Click [here](Assembly/assembly-basics-and-memory_5/README.md) to see the answers.

## What term is used for byte ordering where the least significant bytes are stored first in a memory location?

KSATs: K0210, K0204, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| last in first out | little endian | big endian | first in first out |

Click [here](Assembly/assembly-basics-and-memory_6/README.md) to see the answers.

## Which is the fastest memory component to access?

KSATs: K0204, K0253, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| solid state drive | RAM | cache | registers |

Click [here](Assembly/assembly-basics-and-memory_7/README.md) to see the answers.

## In Assembly, to put data into a register, you use the \_\_\_\_\_ opcode\.

KSATs: K0204, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ins | mov | equ | asn |

Click [here](Assembly/assembly-basics-and-memory_8/README.md) to see the answers.

## In Assembly, for a 64\-bit register rdx, its 32\-bit sub register is \_\_\_\_\.

KSATs: K0213, K0214, K0215, K0202, K0217, K0204, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| eax | dx | edx | rdl |

Click [here](Assembly/assembly-basics-and-memory_9/README.md) to see the answers.

```nasm
1 | mov rax, 0xc0ffee
2 | 
```

## The above Assembly code stores a memory address in the rax register\.  Which instruction would store data at that address?

KSATs: K0213, K0202, K0206, K0207, K0204, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| mov rax, 100 | mov \[rax\], 100 | add rax, 100 | mov 0xc0ffee, 100 |

Click [here](Assembly/assembly-computer-basics_1/README.md) to see the answers.

## What is the smallest unit of data a computer can offer?

KSATs: K0219, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| binary | byte | bit | segment |

Click [here](Assembly/assembly-computer-basics_4/README.md) to see the answers.

## Which of the following commands calls the kernel in Assembly programming?

KSATs: K0206, K0207, K0243, K0242, K0524, K0526, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| mov 0x80 | int 0x80 | call 0x80 | sys\_exit |

Click [here](Assembly/assembly-control-flow_1/README.md) to see the answers.

```nasm
1  | .label1:
2  |     xor rax, rax
3  |     inc rax
4  |     mov rcx, rax
5  |     jmp .label2
6  |     mov rsp, rax
7  | .label2:
8  |     shl rcx, 3
9  |     xchg rcx, rax
10 |     ret
11 | 
```

## In the above Assembly code, which of the following is true?

KSATs: K0202, K0206, K0207, K0204, K0240, K0231, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The line mov rsp, rax is never executed | label1 and label2 are incorrectly defined | shl cannot be executed on the rcx register | jmp can only be used for label1 |

Click [here](Assembly/assembly-control-flow_2/README.md) to see the answers.

## The text section of an Assembly program must begin with what declaration?

KSATs: K0233, K0225, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \_begin | start | section | global \_start |

Click [here](Assembly/assembly-creating-asm_1/README.md) to see the answers.

## In Assembly, which of the following is a valid "high" 8\-bit sub register?

KSATs: K0216, K0217, K0204, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ah | al | ax | eax |

Click [here](Assembly/assembly-data-types-and-gdb-part-2_1/README.md) to see the answers.

## What is the smallest addressable unit in an X86 architecture?

KSATs: K0201, K0219, K0204, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| word | byte | bit | segment |

Click [here](Assembly/assembly-data-types-and-gdb-part-2_2/README.md) to see the answers.

## Which of the following is a valid example of declaring and instantiating a variable in Assembly programming?

KSATs: K0206, K0219, K0204, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| age DW 12345 | age int 12345 | age  DW=12345 | age 12345 |

Click [here](Assembly/assembly-data-types_1/README.md) to see the answers.

## This flag is set if the result of an add would have set bit 33 \(in x86\), or bit 65 \(in x86\_64\)\.

KSATs: K0201, K0213, K0214, K0202, K0238, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Zero Flag \(ZF\) | Carry Flag \(CF\) | Sign Flag \(SF\) | Bit Flag \(BF\) |

Click [here](Assembly/assembly-flags_1/README.md) to see the answers.

## In Assembly with a 32\-bit architecture, what happens when an add or sub operation causes the 33rd bit to be modified?

KSATs: K0201, K0214, K0238, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The Carry Flag is set | the rax register is set to 1 | the unsigned number becomes signed | a virtual 64\-bit register is created |

Click [here](Assembly/assembly-flags_2/README.md) to see the answers.

## what is the purpose of the x86/x86\-64 CALL instruction?

KSATs: K0201, K0691, K0776

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Assign a name to a value so that it can be used later\. | Run a procedure declared elsewhere in code\. | Copy all values pointed to by a source into a destination | Cancel all instructions after the call instruction |

Click [here](Assembly/assembly-instructions-call/README.md) to see the answers.

## What best describes the x86/x86\-64 movs instruction?

KSATs: K0201, K0691, K0767

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Copy the item from the second operand into the first operand\. | Move the item from the second operand into the first operand and set the second operand to zero\. | Copy the item from the second operand into the first operand and change edi and esi\. | Move only one bit from the second operand into the first\. |

Click [here](Assembly/assembly-instructions-movs/README.md) to see the answers.

## What best describes the x86/x86\-64 movzx instruction?

KSATs: K0201, K0691, K0767

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Copy the contents from the second operand into the first operand\. | Move the contents from the second operand into the first operand and set the second operand to zero\. | Copy the contents from the second operand into the first operand and fill the low bits with zeros | Copy the contents from the second operand into the first operand and fill the high bits with zeros |

Click [here](Assembly/assembly-instructions-movzx/README.md) to see the answers.

## What best describes the x86/x86\-64 ret instruction?

KSATs: K0691, K0777

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| returns a value to the calling function\. | returns the stack pointer to zero\. | returns flow to the top of a loop\. | resumes execution flow at the address popped off on the stack\. |

Click [here](Assembly/assembly-instructions-ret/README.md) to see the answers.

```nasm
1 |mov al, 0x01110101
2 |ror al, 3
3 |rol al, 4
```

## What is the value in register al after the above code runs?

KSATs: K0201, K0691, K0767, K0790

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 11100000 | 11101010 | 01010111 | 11010101 |

Click [here](Assembly/assembly-instructions-rotate/README.md) to see the answers.

```nasm
1 |mov eax, 0xA    ;0xA is equivalent to 1010 in binary
2 |shr eax, 2
3 |shl eax, 1
```

## What is the value in register eax after the above code runs?

KSATs: K0201, K0691, K0767, K0784

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1010 | 10 | 100 | 101 |

Click [here](Assembly/assembly-instructions-shifting/README.md) to see the answers.

## The last statement in an Assembly source program should be:

KSATs: K0233, K0225, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| stop | end | return | exit |

Click [here](Assembly/assembly-last-statement/README.md) to see the answers.

```nasm
1 | mov rax, 1
2 | shl rax, 1
3 | shl rax, 3
4 | 
```

## In the above Assembly code, what's contained in the rax register when finished?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 16 | 8 | 1 | 2 |

Click [here](Assembly/assembly-negative-bitwise_1/README.md) to see the answers.

```nasm
1 | mov rax, 2
2 | shr rax, 2
3 | shl rax, 2
4 | 
```

## In the above Assembly code, what's contained in the rax register when finished?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 2 | 1 | 0 | 4 |

Click [here](Assembly/assembly-negative-bitwise_2/README.md) to see the answers.

```nasm
1 | mov rax, 1
2 | mov rcx, 5
3 | 
4 | or rax, rcx
5 | 
```

## In the above Assembly code, what's contained in the rax register when finished?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1 | 5 | 6 | 0 |

Click [here](Assembly/assembly-negative-bitwise_3/README.md) to see the answers.

```nasm
1 | mov rax, 1
2 | mov rcx, 5
3 | and rax, rcx
4 | 
```

## In Assembly, given the above what will rax contain in binary?

KSATs: K0213, K0202, K0206, K0207, K0204, K0233, K0528, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 00000001 | 00000100 | 00000110 | 00000101 |

Click [here](Assembly/assembly-negative-numbers-and-bitwise_1/README.md) to see the answers.

## Instructions like MOV or ADD is called a\(n\) \_\_\_\_\_\_\_\_\_\_ in Assembly\.

KSATs: K0206, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Command | Interrupt | Opcode | Instruction |

Click [here](Assembly/assembly-op-code/README.md) to see the answers.

## When working with the stack in Assembly, which of the following are correct?

KSATs: K0203, K0204, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The stack grows from low memory addresses to high memory addresses | The stack memory addresses are randomly selected by the OS | The stack grows from high memory addresses to low memory addresses | The stack is a first in first out memory structure |

Click [here](Assembly/assembly-stack/README.md) to see the answers.

## The assembler stores all the names and their corresponding values in the \_\_\_\_\_\.

KSATs: K0225, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Special Purpose Register | Symbol Table | Value Map Set | Stack |

Click [here](Assembly/assembly-stores-names/README.md) to see the answers.

## The values 5, 34, 32, 12 are pushed to a data structure and when popped they return 12, 32, 34, 5\. What data structure does this represent?

KSATs: K0203, K0222, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| queue | vector | tree | stack |

Click [here](Assembly/assembly-the-stack_1/README.md) to see the answers.

## What is the common use for two's compliment

KSATs: K0211, K0691

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Quickly detect if a number is even\. | Encrypting data to securely transmit across a network\. | Divide or multiply by powers of 2\. | Convert binary representation of a positive integer to its negative\. |

Click [here](Assembly/assembly-twos-complement/README.md) to see the answers.

## ***C Programming***

```c
1 | #define R 10
2 | #define C 20
3 | 
4 | int* p [R][C];
5 | 
```

## In the above C code snippet, what is p?

KSATs: K0096, K0104, K0105, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| a two\-dimensional array of int pointers | a pointer to a two\-dimensional array of int | a pointer to heap memory of 200 bytes | none of the above |

Click [here](C_Programming/c-2d-array/README.md) to see the answers.

```c
1 | int a = 10/3;
2 | printf("%d",a);
3 | 
```

## What is the output of the above C code snippet?

KSATs: K0096, K0716, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 3\.33 | 3\.0 | 3 | 0 |

Click [here](C_Programming/c-arithmetic-operators_1/README.md) to see the answers.

```c
1 | char str1[5] = {'H', 'e', 'l', 'l', 'o'};
2 | char *str2 = "Hello";
3 | 
```

## What is the difference between str1 and str2 if they are used as arguments to the printf\(\) function with a string format?

KSATs: K0104, K0128, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| str1 may display additional text | str1 will have commas in between the letters when passed as an argument to printf\(\) | str1 will only print out the first letter, H | There is no difference between them |

Click [here](C_Programming/c-array-strings_1/README.md) to see the answers.

```c
1 | int inputBuffer [256] = {0};
2 | 
```

## What is occurring in the C program snippet above?

KSATs: K0104, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Zero is placed only in the first element of the array\. | Zero is placed in all elements of the array\. | null is being placed in all elements of the array\. | Number 0 through 255 are being placed in the array\. |

Click [here](C_Programming/c-arrays_1/README.md) to see the answers.

```c
1 | int x = 5;
2 | if(x = 3)
3 |    printf("it is equal");
4 | else
5 |    printf("it is not equal");
6 | 
```

## Given the above C code snippet, what is the problem?

KSATs: K0107, K0689, K0735, K0754

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 3 is being assigned to x instead of compared\. | if statements require curly braces if there is an else\. | printing strings require a %s specifier\. | the code is logically correct\. |

Click [here](C_Programming/c-assign-vs-equal_1/README.md) to see the answers.

## Which of the following is true when reading and writing files in C?

KSATs: K0689, K0746, K0747

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| File operations are all functions called with a File object using the dot \(\.\) operator | You can only open in either read or write mode, not both | You have to explicitly close files that are opened | You can only write one character at a time |

Click [here](C_Programming/c-basic-file-handling/README.md) to see the answers.

```c
1 | struct Node
2 | {
3 |    int data; 
4 |     struct Node *left;
5 |    struct Node *right;
6 | };
7 |
```

## What common data structure would you use the above structure for in C programming?

KSATs: K0055, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Linked List | Binary Search Tree | Double Linked List | Vector |

Click [here](C_Programming/c-binary-search-tree/README.md) to see the answers.

```c
1 | int x = 2; 
2 | x <<= 1;
```

## What is the value of x after the C program snippet above executes?

KSATs: K0111, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 2 | 1 | 4 | 0 |

Click [here](C_Programming/c-bit-shift-left/README.md) to see the answers.

```c
 ~1011 
```

## What is the result of the above C code snippet?

KSATs: K0111, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1011 | 0100 | 0000 | 1111 |

Click [here](C_Programming/c-bitwise-ones-compliment/README.md) to see the answers.

```c
1 | printf("%d", 12 & 12);
2 | 
```

## What will be printed in the above code?

KSATs: K0111, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 0 | 24 | 12 | 1212 |

Click [here](C_Programming/c-bitwise-operators_1/README.md) to see the answers.

```c
1 | int x = 1;
2 | 
3 | x = x>>2;
4 | 
5 | printf( "%d ", x );
6 | 
```

## What is the output of the above C code snippet?

KSATs: K0111, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 4 | 2 | 0 | 1 |

Click [here](C_Programming/c-bitwise-operators_2/README.md) to see the answers.

```c
 1011 | 0010 
```

## What is the result of the above C code snippet?

KSATs: K0111, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1011 | 0011 | 0010 | 1001 |

Click [here](C_Programming/c-bitwise-or/README.md) to see the answers.

```c
 1011 ^ 0010 
```

## What is the result of the above C code snippet?

KSATs: K0111, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1011 | 0011 | 0010 | 1001 |

Click [here](C_Programming/c-bitwise-xor/README.md) to see the answers.

```c
1 | void processData(int arr[], int size)
2 | {
 3 |   arr[0]++;
4 |   size++;
5 | }
6 | int main()
7 | {
8 |    int x[] = {2,4,6,8};
9 |    int y = 4;
10 |    processData(x, y);
11 | }
```

## Given the above C code snippet, what are the values of x\[0\] and y after execution?

KSATs: K0689, K0739

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| x\[0\] is 2 and y is 4 | x\[0\] is 3 and y is 5 | x\[0\] is 3 and y is 4 | x\[0\] is 0 and y is 4 |

Click [here](C_Programming/c-by-reference/README.md) to see the answers.

## Given the main function defined to receive command line arguments:  int main\( int argc, char \*argv\[\], what will be in argv\[1\] if the following command is execute: \./myProgram blue green red?

KSATs: K0765

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 0 | blue | green | red |

Click [here](C_Programming/c-command-line/README.md) to see the answers.

## During a build, at what stage would the build fail if a function does not exist that is being called?

KSATs: K0140, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| preprocess | compile | link | deploy |

Click [here](C_Programming/c-compile-link/README.md) to see the answers.

```c
1  | int x = 0;
2  | switch(x) 
3  | {
4  |    case 0: 
5  |      printf("Hello");
6  |    case 1: 
7  |      printf("hi"); 
8  |      break;
9  | }
10 | 
```

## What is the output of the above code snippet?

KSATs: K0689, K0735, K0755

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Hello | Hi | Hellohi | Nothing prints |

Click [here](C_Programming/c-conditional-statements_1/README.md) to see the answers.

## Which of the following is a valid ASCII char declaration in C?

KSATs: K0096, K0128, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| char x = '\\n' | char x = "A"; | char x = \-3; | None of the above |

Click [here](C_Programming/c-data-types_1/README.md) to see the answers.

```c
1 | char *ptr1;
2 | printf("%d %d", sizeof(ptr1), sizeof(*ptr1));
3 | 
```

## What is the output of the above C code snippet? //This question is compiled on 32 bit DEV\-C\+\+

KSATs: K0096, K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1 1 | 2 2 | 4 1 | 4 4 |

Click [here](C_Programming/c-demo-lab-1-2-c_1/README.md) to see the answers.

## Which of the following operator, known as a dereference, can be used to access value at address stored in a pointer variable?

KSATs: K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| & | \# | \* | \-> |

Click [here](C_Programming/c-dereference/README.md) to see the answers.

```c
1 | unsigned int x = 0xabcd1234;
2 | char *c = (char*) &x;
3 | printf("%x", *c)
4 | 
```

## In the above C code snippet, what is printed assuming the machine is big endian?

KSATs: K0108, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ab | cd | 12 | 34 |

Click [here](C_Programming/c-detect-endianness/README.md) to see the answers.

```c
1 | const int a = 10;
2 | printf("%d",++a);
3 | 
```

## What is the output of the above C code snippet?

KSATs: K0102, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 11 | 10 | Compiler error | 0 |

Click [here](C_Programming/c-directives_2/README.md) to see the answers.

## Which of the following is executed by the preprocessor in C?

KSATs: K0105, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \#include<stdio\.h> | return 0 | void main \(int argc , char \*\* argv\) | None of above |

Click [here](C_Programming/c-directives_3/README.md) to see the answers.

## To utilize a header file like stdio\.h in C programming, you use the \_\_\_\_\_\_\_\_\_\_ preprocessor directive\.

KSATs: K0105, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \#define | \#using | \#import | \#include |

Click [here](C_Programming/c-directives_4/README.md) to see the answers.

```c
1 | enum week{Mon, Tue, Wed, Thur, Fri, Sat, Sun}; 
2 | printf(Fri); 
```

## What is printed in the C program snippet above?

KSATs: K0126, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Fri | 4 | 0 | 5 |

Click [here](C_Programming/c-enum-values/README.md) to see the answers.

## Which function would you use to point to the 10th byte in a file in C?

KSATs: K0689, K0750

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| seek\(\) | fgetc\(\) | move\(\) | fseek\(\) |

Click [here](C_Programming/c-file-navigation/README.md) to see the answers.

## Which c function call opens the file 'example\.txt' in write mode and overwrites it if it exists?

KSATs: K0689, K0747, K0748

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| fopen\("example\.txt", "a\+"\) | open\("example\.txt"\) | fopen\("example\.txt","w"\) | printf\("example\.txt"\) |

Click [here](C_Programming/c-file-overwrite/README.md) to see the answers.

```c
1 | FILE* fp = fopen("file.txt", "r");
2 | fseek(fp, 0L, SEEK_END);
3 | int res = ftell(fp);
```

## What does the variable res contain after the above snippet executes?

KSATs: K0689, K0749

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The last character in the file | The file size in bytes | The number of lines in the file | The address of the last character in the file |

Click [here](C_Programming/c-file-size/README.md) to see the answers.

```c
1 | 
2 | int a = 10.5;
3 | printf("%d",a);
4 | 
```

## What is the output of the above C code snippet?

KSATs: K0096, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10\.5 | 10 | 0 | 5 |

Click [here](C_Programming/c-formatted-io_1/README.md) to see the answers.

```c
1 | double addValues(double, double);
2 | 
```

## The above C code is an example of a:

KSATs: K0009, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| function | method | prototype | procedure |

Click [here](C_Programming/c-function-basics_1/README.md) to see the answers.

```c
1 | int getTotal(int a, int b) 
 2 | { 
 3 |     return a + b; 
 4 | } 
 5 |   
 6 | int main() 
 7 | { 
 8 |     int (*tot)(int, int) = &getTotal; 
 9 |     int x = (*tot)(10, 20);
 10 |     return 0; 
 11 | }
```

## What is occuring on line 8 in the above C code?

KSATs: K0689, K0760

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The getTotal function is being invoked\. | The getTotal function is being redefined to return a int pointer\. | A function pointer is being created for getTotal\. | getTotal is being allocated in the heap\. |

Click [here](C_Programming/c-function-pointers/README.md) to see the answers.

```c
1  | void main()
2  | {
3  |    fun(3);
4  | }
5  | 
6  | void fun(int n)
7  | {
8  |    for(int i = n;i > 0; i--)
9  |      printf("GeeksQuiz");
10 | }
11 | 
```

## Why does the above program cause a compile error?

KSATs: K0009, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The code has a syntax error in for loop header\. | fun in the main function has yet to be defined\. | fun is an invalid function name\. | n is not initialized in the for loop header\. |

Click [here](C_Programming/c-functions_5/README.md) to see the answers.


---

### Begin C-GROUP_1 Questions

Please refer to the code snippet below for the following 14 question(s).

```c
1  | #include <stdio.h>
2  | #include <stdlib.h>
3  | 
4  | #define y 10
5  | 
6  | float getQuotient(int, int);
7  | 
8  | int main()
9  | {
10 |     int x = rand();
11 | 
12 |     float z = getQuotient(x,y);
13 |     
14 |     float* p = (float *) malloc(y * sizeof(float));
15 |     
16 |     p[0] = z;
17 |     
18 |     printf("Result is %f", *p);
19 |     
20 |     return 0;
21 | }
22 | 
23 | float getQuotient(int numerator, int denominator)
24 | {
25 |     return (float)numerator/denominator;
26 | }
27 | 
```

## Referring to the above code snippet, which of the following lines of code contain a type cast?

KSATs: K0096, K0106, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 4 | 12 | 25 | 23 |

Click [here](C_Programming/c-group_1/c-casting_1/README.md) to see the answers.

## Which line of code contains a preprocessor directive that creates a named constant?

KSATs: K0105, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 2 | 10 | 4 | 16 |

Click [here](C_Programming/c-group_1/c-directives_1/README.md) to see the answers.

## Referring to the above code snippet, which line of code executes the getQuotient function?

KSATs: K0009, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 6 | 12 | 23 | 25 |

Click [here](C_Programming/c-group_1/c-functions_1/README.md) to see the answers.

## Which line of code contains a function prototype?

KSATs: K0009, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10 | 6 | 12 | 19 |

Click [here](C_Programming/c-group_1/c-functions_2/README.md) to see the answers.

## What is the return type of the getQuotient function?

KSATs: K0007, K0012, K0009, K0096, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| float | int | void | NULL |

Click [here](C_Programming/c-group_1/c-functions_3/README.md) to see the answers.

## What would happen if on line 12 you passed two floats instead of two ints?

KSATs: K0010, K0009, K0096, K0106, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The values passed would be floating point numbers | The values passed would be truncated and passed as integers | The values would be rounded to the nearest int and passed | Compile error |

Click [here](C_Programming/c-group_1/c-functions_4/README.md) to see the answers.

## In the above image, which line of code places some data in heap memory?

KSATs: K0122, K0121, K0689, K0757

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10 | 12 | 16 | 18 |

Click [here](C_Programming/c-group_1/c-heap_1/README.md) to see the answers.

## Which line of code is allocating heap memory?

KSATs: K0122, K0121, K0689, K0757

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10 | 12 | 14 | 27 |

Click [here](C_Programming/c-group_1/c-heap_2/README.md) to see the answers.

## For memory management, what is missing?

KSATs: K0103, K0121, K0689, K0758

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| p\[0\] = NULL; | p = NULL; | delete \*p; | free\(p\) |

Click [here](C_Programming/c-group_1/c-memory-management_1/README.md) to see the answers.

## How many elements are in the array pointed to by p?

KSATs: K0104, K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10 | 40 | 80 | 8 |

Click [here](C_Programming/c-group_1/c-pointers-arrays_1/README.md) to see the answers.

## Which of the following variables contain a memory address?

KSATs: K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| p | z | y | All the above |

Click [here](C_Programming/c-group_1/c-pointers_1/README.md) to see the answers.

## Referring to the above code snippet, on which line of code is the pointer dereferenced?

KSATs: K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 16 | 18 | 12 | 14 |

Click [here](C_Programming/c-group_1/c-pointers_2/README.md) to see the answers.

## How many local variables are created in the main function?

KSATs: K0011, K0009, K0093, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 0 | 2 | 3 | 4 |

Click [here](C_Programming/c-group_1/c-scope_1/README.md) to see the answers.

## Which of the following variables declare memory in stack memory?

KSATs: K0120, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| z | p | denominator | All the above |

Click [here](C_Programming/c-group_1/c-stack_1/README.md) to see the answers.


### End C-GROUP_1 Questions

---

## Which of these data structures has a constant search time for the worst case scenario?

KSATs: K0052, K0104, K0110, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Array | Linked list | Hash tables | Stack |

Click [here](C_Programming/c-hashing/README.md) to see the answers.

## How do you prevent multiple dependencies from causing compilation errors?

KSATs: K0095, K0101, K0105, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Only \#include source files | \#include will overwrite the previous \#include if they are the same file\. | Use the preprocessor directives \#ifndef, \#define, and \#endif to check if a header file was already included | Only one file can be \#include into a file\. |

Click [here](C_Programming/c-header-files_1/README.md) to see the answers.

## What should not be included in C header files?

KSATs: K0101, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Function prototype | Include guards | Type and struct definitions | Function implementation |

Click [here](C_Programming/c-header-files_2/README.md) to see the answers.

```c
   1 | int x = 5;
 2 |     x++;
 3 |     printf("%d",x++);
 
```

## What is printed in the above snippet?

KSATs: K0689, K0764

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 5 | 6 | 7 | d5 |

Click [here](C_Programming/c-increment-operator/README.md) to see the answers.

## Which of the following are valid loop keywords in C?

KSATs: K0102, K0107, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| for | do while | repeat | both a and b |

Click [here](C_Programming/c-loops_1/README.md) to see the answers.

```c
1 | int i,j;
2 | for(i=0,j=0;i<5;i++)
3 | {
4 |    printf("%d%d--",i,j);
5 | }
6 | 
```

## What is the output of the above C code snippet?

KSATs: K0107, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 0\-\-01\-\-12\-\-23\-\-34\-\- | 00\-\-10\-\-20\-\-30\-\-40\-\- | Nothing is printed | 00\-\-01\-\-02\-\-03\-\-04\-\- |

Click [here](C_Programming/c-loops_2/README.md) to see the answers.

## Which of the following is true about the 'main' function in C programming?

KSATs: K0093, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| There can be multiple 'main' functions in a program\. | The 'main' function does not have a return type\. | All execution begins and ends in the 'main' function\. | 'main' is an optional function to have in a program\. |

Click [here](C_Programming/c-main-function/README.md) to see the answers.

```c
1 | int *x = malloc(sizeof(int));
2 | 
```

## In the above statement, where is the memory pointed to by x allocated?

KSATs: K0103, K0119, K0122, K0121, K0689, K0735, K0757

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Stack | Registry | Heap | Cache |

Click [here](C_Programming/c-memory-management_2/README.md) to see the answers.

## What C command is use to allocate memory dynamically?

KSATs: K0122, K0689, K0735, K0757

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| malloc | alloc | new | memcpy |

Click [here](C_Programming/c-memory-management_3/README.md) to see the answers.

```c
1 | int x [10];
2 | int *y = malloc(sizeof(x));
3 | 
```

## Given the above C code snippet and given an int is 4 bytes, how many bytes are being allocated for the y pointer?

KSATs: K0104, K0103, K0689, K0735, K0757

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 4 | 40 | 80 | 10 |

Click [here](C_Programming/c-memory-management_4/README.md) to see the answers.

```c
1 | int x [10];
2 | int *y = malloc(sizeof(x));
3 | 
```

## Given the above C code snippet, where is the memory being allocated with the malloc command?

KSATs: K0103, K0119, K0122, K0121, K0689, K0735, K0757

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| heap | stack | cache | registers |

Click [here](C_Programming/c-memory-management_5/README.md) to see the answers.

```c
1  |  int ages[] = {23,34,31,54,61,14,8};
2  |     int *aPtr = ages, *end = &ages[6];
3  |     int val = 0;
4  |     while(aPtr < end)
5  |     {
6  |         val += *aPtr;
7  |         aPtr += 2;
8  |     } 
9  |  }
10 | 
```

## What is the value of val after the above C code snippet executes?

KSATs: K0689, K0761

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 115 | 217 | 231 | 97 |

Click [here](C_Programming/c-pointer-arithmetic/README.md) to see the answers.

```c
1 | struct site
2 | {
3 |    char name[10];
4 |    int no_of_pages;
5 | };
6 | struct site *ptr;
7 | printf("%d ", ptr->no_of_pages);
8 | printf("%s", ptr->name);
9 | 
```

## Why would you receive a compile error for the above snippet of code?

KSATs: K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| There is a semi\-colon after the struct declaration\. | struct pointers do no support the \-> operator\. | ptr hasn't been initialized\. | pointers cannot be declared for a struct type\. |

Click [here](C_Programming/c-pointers-and-arrays_1/README.md) to see the answers.

```c
1  | void main()
2  | {
3  |    double scores[20];
4  |    fun(scores);
5  | }
6  | 
7  | void fun(double s[])
8  | {
9  |    // do something here
10 | }
11 | 
```

## For the above C code snippet, what is contained in s?

KSATs: K0104, K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| First value of the score array | Base address of the score array | Entire contents of score array | A copy of the entire score array |

Click [here](C_Programming/c-pointers-and-arrays_2/README.md) to see the answers.

```c
1 | 
2 | char *p = NULL;
3 | 
4 | printf("%c ", *p);
5 | 
```

## Why does the above C code snippet cause a run\-time error?

KSATs: K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| You cannot initialize a pointer to NULL\. | null should be all lower case\. | You cannot dereference a NULL pointer | %c is not a valid specifier |

Click [here](C_Programming/c-pointers-and-arrays_3/README.md) to see the answers.

```c
1 | int values [] = {1, 2, 3, 4, 5};
2 | 
3 | printf("%d",*values);
4 | 
```

## Given the above C snippet, what is the output or result?

KSATs: K0104, K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| syntax error | run\-time error | 1 is printed | prints address of array |

Click [here](C_Programming/c-pointers-and-arrays_4/README.md) to see the answers.

```c
1 | int a = 10;
2 | int *c = &a;
3 | printf("%d",c);
4 | 
```

## What is printed for the above code?

KSATs: K0096, K0103, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 10 | the address of a | the address of c | Nothing |

Click [here](C_Programming/c-pointers-arrays_2/README.md) to see the answers.

```c
1 | char *text = "This is a test"; 
2 | text += 2;
3 | 
```

## Given the above code, what is the dereferenced value of text?

KSATs: K0096, K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| is is a test | i | an address | This is a test |

Click [here](C_Programming/c-pointers-arrays_3/README.md) to see the answers.

```c
1 | int x = 10;
2 | int *y = &x;
3 | (*y)++;
4 | x++;
5 | printf("%d\n",*y);
6 | printf("%d\n",x);
7 | 
```

## Given the above C code snippet, what is the output or result?

KSATs: K0096, K0103, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 11<br>11 | 12<br>12 | 11<br>12 | 12<br>11 |

Click [here](C_Programming/c-pointers_3/README.md) to see the answers.

## Which of the following is a preprocessor instruction?

KSATs: K0689, K0762

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \.import | \#include | &malloc | \- link |

Click [here](C_Programming/c-preprocessor/README.md) to see the answers.

```c
1 | float *payIncrease(float factor)
2 | {
3 |     float salary[] = {24.5, 33.1, 17.2, 18.4};
4 |     for(int i = 0; i < 4; i++)
5 |         salary[i] *= factor;
6 |     return salary;
7 | }
```

## Given the above C function, what logical problem exists?

KSATs: K0689, K0740

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| C does not support a return type of a pointer | The function returns the address of a local array | The for loop has a one\-off error | The array should have a declared size to prevent overflow |

Click [here](C_Programming/c-return-address/README.md) to see the answers.

```c
1 | int *ptr1 = malloc(sizeof(int));
2 | 
```

## Where is ptr1 stored?

KSATs: K0103, K0120, K0122, K0121, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Cache | The stack | The heap | None of the above |

Click [here](C_Programming/c-stack-based-memory_1/README.md) to see the answers.

```c
1 | int x = 2; 
2 | 
```

## Where is the memory for x being allocated in the above C snippet?

KSATs: K0120, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| registers | heap | disk | stack |

Click [here](C_Programming/c-stack-memory/README.md) to see the answers.

## In C programming, what command releases memory that was created on the heap?

KSATs: K0122, K0121, K0689, K0758

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| memfree | free | dealloc | delete |

Click [here](C_Programming/c-standard-library-functions_1/README.md) to see the answers.

```c
1 | int x = 2; 
2 | 
```

## When is the memory for x being allocated in the above C snippet?

KSATs: K0122, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Dynamically, at run time | Statically, at compile time | Virtually, when requested | Preliminary, just before accessed |

Click [here](C_Programming/c-static-allocation/README.md) to see the answers.

## Which string function copies bytes from one char \* array to another char \* array and also limits number of bytes copied?

KSATs: K0187

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| strcpy\. | strstr\. | strncpy\. | strncat\. |

Click [here](C_Programming/c-string-functions_1/README.md) to see the answers.

```c
1 | char name[]="Cppbuz";
2 | int len;
3 | int size;
4 | len = strlen(name);
5 | size = sizeof(name);
6 | printf("%d,%d",len,size);
7 | 
```

## What is the output of the above C code snippet?

KSATs: K0096, K0104, K0188, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 6,6 | 6,7 | 7,7 | 0,0 |

Click [here](C_Programming/c-strings_1/README.md) to see the answers.

```c
1 | char s[20] = "Hello\0Hi";
2 | 
3 | printf("%s", s);
4 | 
```

## What is the output of the above C code snippet?

KSATs: K0128, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Hello\\0Hi | Hello Hi | Hello | Hi |

Click [here](C_Programming/c-strings_2/README.md) to see the answers.

```c
1 | struct score{
2 |    int a;
3 | };
4 | 
```

## For the above struct declaration, which is the proper way to declare a variable for the struct?

KSATs: K0124, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| int score; | struct score myScore; | score\.a = 100; | score myScore; |

Click [here](C_Programming/c-struct-declaration_1/README.md) to see the answers.

```c
1 | struct person {
2 |   char *name;
3 |   int age;
4 | } artist;
5 | 
```

## Given the above C code snippet, how would you initialize name in the struct person?

KSATs: K0103, K0124, K0128, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| name\.artist = "Picasso"; | person\.name = "Picasso"; | artist\->name = "Picasso"; | artist\.name = "Picasso"; |

Click [here](C_Programming/c-struct-format_1/README.md) to see the answers.

```c
1 | struct myGrade{
2 |    int a;
3 | };
4 | 
5 | struct myGrade grade1={1};
6 | struct myGrade grade2 = grade1;
7 | 
```

## In the above C code snippet, what is happening on the last line?

KSATs: K0124, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| grade2 is created as an alias to grade1 | grade2 is a pointer pointing to grade1 | the contents of grade1 is being used to create and initialize grade2 | grade2 is replacing grade1 in stack memory |

Click [here](C_Programming/c-structs_1/README.md) to see the answers.

```c
1  | struct Person {
2  |    char name[20];
3  |    int age;
4  | }p;
5  | 
6  | void main()
7  | {
8  |    struct Person * p=(struct Person p)malloc(sizeof(Person));
9  |    // Here...
10 | }
11 | 
```

## Given the above code, how would you assign a value to name in the Person struct object?

KSATs: K0103, K0124, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Person\->name = "Ted" | p\.name = "Ted" | p\->name = "Ted" | Person\.name = "Ted" |

Click [here](C_Programming/c-structs_2/README.md) to see the answers.

## When using threads, what does the pthread\_self\(\) function do?

KSATs: K0185, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| it returns the address of the current thread | it makes a copy of the current thread | it returns the id of the current thread | it creates a new thread instance |

Click [here](C_Programming/c-thread-id/README.md) to see the answers.

```c
1 | int x = 10;
2 | int y = 3;
3 | 
4 | float z = (float)x/y;
5 | 
```

## Why is a cast necessary in the above code snippet?

KSATs: K0096, K0106, K0689, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| because int cannot be assigned to float variables | otherwise floating point precision would be lost in the division | float casting is required for int division | to reduce the length of the division result |

Click [here](C_Programming/c-type-conversions_1/README.md) to see the answers.

```c
1 | typedef unsigned int counter;
2 | 
```

## What does this code do?

KSATs: K0127, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Creates an unsigned int named counter\. | Creates a new type named counter that acts like an unsigned int\. | Creates a variable named counter that has no type defined until runtime | Creates a new struct that can only hold unsigned int |

Click [here](C_Programming/c-typedef_1/README.md) to see the answers.

```c
1 | union Data {
2 |    int i;
3 |    float f;
4 |    char str[20];
5 | };
```

## Given the above C union declaration, which is the proper way to declare a Data variable?

KSATs: K0125, K0689

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| union Data mydata; | Data mydata; | Data\.union\(mydata\); | Data union mydata; |

Click [here](C_Programming/c-unions/README.md) to see the answers.

## Which of the following is true about a return statement in the body of a function?

KSATs: K0689, K0738

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| All functions must have a return statement\. | void functions must return void or null\. | void functions don't need a return statement | the item returned cannot be a memory address |

Click [here](C_Programming/c-void-return-type/README.md) to see the answers.

## What is the purpose the C library function calloc?

KSATs: K0689, K0823

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| allocates memory and sets allocated memory to zero | allocates memory and sets allocated memory to NULL | allocates memory but does not modify the memory | captures allocated memory created by the malloc function |

Click [here](C_Programming/c-zero-out-memory/README.md) to see the answers.

## ***Data Structures***

## Which of the following is a disadvantage of using a linked\-list over an array in C for the same amount of data?

KSATs: K0060, K0700

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| linked\-lists use more memory | linked\-lists cannot be randomly accessed | linked\-lists nodes do not use contiguous memory | all the above |

Click [here](Data_Structures/data-structure-pitfalls/README.md) to see the answers.

## Which of the following is a proper behavior for a queue?

KSATs: K0061, K0700, K0752

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Items can be added to the front of the queue if it's implemented with a linked\-list\. | Items are removed from the front of the queue\. | A queue is a Last In, First Out data structure\. | Both B & C are correct\. |

Click [here](Data_Structures/data-structure-queue/README.md) to see the answers.

## What does the last node of a multi\-node circularly linked list point to?

KSATs: K0058, K0700

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The first node | NULL | Itself | The previous node |

Click [here](Data_Structures/data-structures-circularly-linked-lists/README.md) to see the answers.

```c
1  |  struct Node {
2  |    int data;
3  |    struct Node *next;
4  |    struct Node *prev;
5  |  };
6 | 
```

## The above C struct definition is ideal for what type of Data Structure?

KSATs: K0057, K0700

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Linked List | Doubly Linked List | Binary Tree | Graph |

Click [here](Data_Structures/data-structures-doubly-linked-lists/README.md) to see the answers.

## Which of the following is a proper behavior for a stack?

KSATs: K0061, K0700, K0751

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Items can be removed anywhere in the stack if it's implemented with a linked\-list\. | Only the last item added can be removed from the stack\. | A stack is a last in last out data structure\. | Both B & C are correct\. |

Click [here](Data_Structures/data-structures-stack/README.md) to see the answers.

```c
1 | class Node:
 2 |     def __init__(self, data):
 3 |         self.data = data
 4 |         self.children = []

```

## The above class is ideal for what type of data structure?

KSATs: K0700, K0753

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| linked\-list\. | doubly\-linked list\. | general tree | binary tree |

Click [here](Data_Structures/data-structures-tree/README.md) to see the answers.

## In a Weighted Graph, the weight is applied to each of the graph's \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

KSATs: K0059, K0700

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| nodes | vertices | levels | edges |

Click [here](Data_Structures/data-structures-weighted-graphs/README.md) to see the answers.

## ***Git***

## Assume you have made changes to local files and you want to update your local branch with those files, which git commands would you use?

KSATs: K0724

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| save and update | add and commit | add and push | save and commit |

Click [here](Git/git-branch-update/README.md) to see the answers.

## What does the 'git diff' command do?

KSATs: K0725

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Shows unstaged changes between your branch and working directory\. | Shows unstaged changes between your branch and previous version of branch\. | Shows changes since last commit\. | Shows changes between different master branch versions\. |

Click [here](Git/git-changes/README.md) to see the answers.

## What does the command 'git clone https://myRepo' do?

KSATs: K0726

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Creates a new repo at https://myRepo\. | Clones the master repo to the server located at https://myRepo\. | Clones the repo located at https://myRepo onto remote machine\. | Clones the repo located at https://myRepo onto local machine\. |

Click [here](Git/git-clone/README.md) to see the answers.

## Which of the following examples is the proper way to create and check out a branch in Git?

KSATs: K0722

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| git create \-b my\-branch | git checkout my\-branch | git checkout \-b my\-branch | git branch \-n my\-branch |

Click [here](Git/git-create-branch/README.md) to see the answers.

## If you are in the Master branch and want to merge in a branch called 'add\_git\_questions', what command would you use?

KSATs: K0723

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| git commit add\_git\_questions | git merge master | git merge add\_git\_questions | git master merge add\_git\_questions |

Click [here](Git/git-merge/README.md) to see the answers.

## What is one purpose of a Git repository?

KSATs: K0720

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| It validates logic in your programs\. | It provides syntax checking, compiles, and links programs | It provides version control for project files\. | It creates automated unit tests stubs from pydoc documents |

Click [here](Git/git-purpose/README.md) to see the answers.

## Which of the following commands would be used to snapshot a Git repository?

KSATs: K0721

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 'git add' followed by a 'git commit' | 'git commit' followed by a 'git snapshot' | 'git commit' followed by a 'git tag' | 'git revision' followed by a 'git push' |

Click [here](Git/git-snapshot/README.md) to see the answers.

## Which of the following describes a Centralized Workflow?

KSATs: K0001

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Flow that uses a central repository to serve as the single point\-of\-entry for all changes to the project\. | Flow where all feature development should take place in a dedicated branch instead of the master branch\. | Flow that assigns very specific roles to different branches and defines how and when they should interact\. | Flow where instead of using a single server\-side repository, it gives every developer their own server\-side repository\. |

Click [here](Git/git-workflow_1/README.md) to see the answers.

## Which of the following describes a GitFlow Workflow?

KSATs: K0001

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Flow that uses a central repository to serve as the single point\-of\-entry for all changes to the project | Flow where all feature development should take place in a dedicated branch instead of the master branch\. | Flow that assigns very specific roles to different branches and defines how and when they should interact\. | Flow where instead of using a single server\-side repository, it gives every developer their own server\-side repository\. |

Click [here](Git/git-workflow_2/README.md) to see the answers.

## ***Networking***

## Which of the following describes Classless Inter\-Domain Routing \(CIDR\) notation:

KSATs: K0088

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The process where a network device assigns a public address to one or more computers inside a private network | An IP addressing scheme that improves the allocation of IP addresses\. It replaces the old system based on classes A, B, and C | A method to dynamically assign an IP address and other network configuration parameters to each device on a network so they can communicate with other IP networks | A communication protocol used for discovering the link layer address, such as a MAC address, associated with a given internet layer address |

Click [here](Networking/networking-CIDR/README.md) to see the answers.

## Which of the following describes Network Address Translation \(NAT\):

KSATs: K0087

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The process where a network device assigns a public address to one or more computers inside a private network | An IP addressing scheme that improves the allocation of IP addresses\. It replaces the old system based on classes A, B, and C | A method to dynamically assign an IP address and other network configuration parameters to each device on a network so they can communicate with other IP networks | A communication protocol used for discovering the link layer address, such as a MAC address, associated with a given internet layer address |

Click [here](Networking/networking-NAT/README.md) to see the answers.

## This protocol is similar to DNS because of the way it maps layer 3 to layer 2 addresses\.

KSATs: K0066, K0612, K0611, K0613

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| UDP | HTTP | ARP | IPV4 |

Click [here](Networking/networking-arp_1/README.md) to see the answers.

## Which class of IPV4 addresses have the first bit set to 0 and first octet in range of 1 \- 127?

KSATs: K0081

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Class A | Class B | Class C | Class D |

Click [here](Networking/networking-class-A-address/README.md) to see the answers.

## The Logical Link Control \(LLC\) layer is a sub\-layer to which Open System Interconnection \(OSI\) layer?

KSATs: K0066

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Data Link | Network | Session | Physical |

Click [here](Networking/networking-data-link-layer/README.md) to see the answers.

## The Dynamic Host Configuration Protocol \(DHCP\) is a network management protocol \_\_\_\_\_\_\_\_\_\_\.

KSATs: K0616

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| used on networks whereby a server dynamically assigns an IP address to each device\. | used to copy files from one host to another\. |  that defines a mechanism for communication between browser and the web server\. | naming system for devices connected to the Internet and associates devices with domain names\. |

Click [here](Networking/networking-dhcp/README.md) to see the answers.

## Upon bootup, a particular network device was configured with 0\.0\.0\.0 so it asked for and received an IP from the \_\_\_\.

KSATs: K0089, K0611, K0613, K0616

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Router | DNS Server | ARP Server | DHCP server |

Click [here](Networking/networking-dhcp_1/README.md) to see the answers.

## What allows Bob to enter 'www\.example\.com' into his browser instead of 8\.8\.8\.8 to view a website?

KSATs: K0613

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| DNS Servers | ARP Servers | TCP Servers | DHCP Servers |

Click [here](Networking/networking-dns-servers_1/README.md) to see the answers.

## This protocol follows the classic client\-server model with its stateless sessions typically consisting of: 1\) a client establishing a TCP connection, 2\) a client sending a request and then waits for a response, and 3\) a server sending a response to the client's request\.

KSATs: K0065, K0612, K0611, K0614

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| hypertext transfer protocol | user datagram protocol | simple mail transfer protocol | address resolution protocol |

Click [here](Networking/networking-http_1/README.md) to see the answers.

## A server sends a SYN\-ACK to a client's SYN request\. This is step 2 of a 3\-way process of what communication protocol?

KSATs: K0064

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| File Transfer Protocol \(FTP\) | Transmission Control Protocol \(TCP\) | Open Systems Interconnection \(OSI\) | Dynamic Host Configuration Protocol \(DHCP\) |

Click [here](Networking/networking-intro-to-tcp_1/README.md) to see the answers.

## An IPV4 octet's number ranges from:

KSATs: K0081

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1 to 254 | 0 to 196 | 10 to 255 | 0 to 255 |

Click [here](Networking/networking-ipv4-addresses_1/README.md) to see the answers.

## Which of the following are valid IPV6 addresses?

KSATs: K0082

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 2001:0db8:0000:0000:0000:ff00:0042:8329 | 2001:db8:0:0:0:ff00:42:8329 | 2001:db8::ff00:42:8329 | All of the above |

Click [here](Networking/networking-ipv6-addresses-2/README.md) to see the answers.

## The size of an IPv6 address differs from an IPv4 address because it has \_\_\_ compared to IPv4's \_\_\_?

KSATs: K0082, K0081

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 32 bits, 128 bits | 128 bits, 32 bits | 64 bits, 32 bits | 100 bits, 64 bits |

Click [here](Networking/networking-ipv6-addresses_1/README.md) to see the answers.

## In networking, what Layer 2 device connects two independent physical networks, does not perform error checking and does not have a buffer?

KSATs: K0066, K0089

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Hub | Bridge | Switch | Repeater |

Click [here](Networking/networking-layer-2-devices_1/README.md) to see the answers.

## In networking, which Python function converts 32\-bit positive integers from network to host byte order?

KSATs: K0091

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| socket\.ntohl\(x\) | socket\.ntohs\(x\) | socket\.htonl\(x\) | socket\.htons\(x\) |

Click [here](Networking/networking-network-byte-order_1/README.md) to see the answers.

## In networking, which Python function converts 16\-bit positive integers from network to host byte order?

KSATs: K0091

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| socket\.ntohl\(x\) | socket\.ntohs\(x\) | socket\.htonl\(x\) | socket\.htons\(x\) |

Click [here](Networking/networking-network-byte-order_2/README.md) to see the answers.

## Jimmy sent a package to Dean and noticed the source port was 51234 and the destination port was 666 on one of the TCP packets\.  The difference between these two port types is \_\_\_\.

KSATs: K0064, K0065, K0610

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| port 51234 is assigned by IANA whereas 666 is never assigned\. | port 51234 is a registered port whereas 666 is an ephemeral port\. | other than the numbers, there is no difference\. | port 51234 is an ephemeral port whereas 666 is a well\-known port\. |

Click [here](Networking/networking-networks-and-ports_1/README.md) to see the answers.

## In networking and typically reserved for servers, which port numbers are considered "privileged" ports?

KSATs: K0610

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| < 1024 | < 2048 | > = 1024 | > = 2048 |

Click [here](Networking/networking-networks-and-ports_2/README.md) to see the answers.

## Which Python function waits for a client to connect on the port?

KSATs: K0068, K0618

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| close\(\) | listen\(\) | accept\(\) | recv\(\) |

Click [here](Networking/networking-order-of-operations_1/README.md) to see the answers.

## Which of the following Python functions enables data transfer?

KSATs: K0068, K0618

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| bind\(\) | socket\(\) | connect\(\) | send\(\) |

Click [here](Networking/networking-order-of-operations_2/README.md) to see the answers.

## The first \_\_\_ byte\(s\) of a physical machine address, also known as a/an \_\_\_\_\_ address, identify the manufacturer\.

KSATs: K0086

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1, IP | 2, DHCP | 3, MAC | 2, MAC |

Click [here](Networking/networking-order-of-operations_3/README.md) to see the answers.

## Which of these is not an application layer protocol?

KSATs: K0064, K0066, K0612, K0614

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| HTTP | SMTP | FTP | TCP |

Click [here](Networking/networking-osi-layer-7_1/README.md) to see the answers.

## Which of the following describe OSI layer 7?

KSATs: K0075

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The data link layer that is responsible for the node to node delivery of the message\. The main function of this layer is to make sure data transfer is error\-free from one node to another, over the physical layer\. | The network layer that transmits data from one host to the other located in different networks\. It also takes care of packet routing i\.e\. selection of the shortest path to transmit the packet, from the number of routes available | The transport layer that provides services to application layer and takes services from network layer\. The data in the transport layer is referred to as Segments | The application layer which is implemented by the network applications\. These applications produce the data, which has to be transferred over the network\. |

Click [here](Networking/networking-osi-layer-seven/README.md) to see the answers.

## Which of the following describe OSI layer 3?

KSATs: K0071

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The data link layer that is responsible for the node to node delivery of the message\. The main function of this layer is to make sure data transfer is error\-free from one node to another, over the physical layer\. | The network layer that transmits data from one host to the other located in different networks\. It also takes care of packet routing i\.e\. selection of the shortest path to transmit the packet, from the number of routes available | The transport layer that provides services to application layer and takes services from network layer\. The data in the transport layer is referred to as Segments | The application layer which is implemented by the network applications\. These applications produce the data, which has to be transferred over the network\. |

Click [here](Networking/networking-osi-layer-three/README.md) to see the answers.

## Which of the following describe OSI layer 2?

KSATs: K0073

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The network layer that transmits data from one host to the other located in different networks\. It also takes care of packet routing i\.e\. selection of the shortest path to transmit the packet, from the number of routes available | The data link layer that is responsible for the node to node delivery of the message\. The main function of this layer is to make sure data transfer is error\-free from one node to another, over the physical layer\. | The transport layer that provides services to application layer and takes services from network layer\. The data in the transport layer is referred to as Segments | The application layer which is implemented by the network applications\. These applications produce the data, which has to be transferred over the network\. |

Click [here](Networking/networking-osi-layer-two/README.md) to see the answers.

## One difference between a switch and a hub is that the hub operates at the \_\_\_ layer of the Open Systems Interconnection \(OSI\) model\.

KSATs: K0066, K0089

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Physical | Network | Data Link | Transport |

Click [here](Networking/networking-osi-layers_1/README.md) to see the answers.

## What is one difference between the Open Systems Interconnection \(OSI\) Network and Transport Layers?

KSATs: K0066

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The Network layer is divided into two sublayers: Logical Link Control \(LLC\) and Media Access control \(MAC\); whereas the Transport layer performs segmentation and flow control\. | The Network layer takes information it receives from the session layer and passes it to the Transport layer; the transport layer then passes this information to the data link layer\. | The Network layer is responsible for routing and addressing whereas the Transport layer handles end\-to\-end delivery of a message\. | The Network layer provides connection oriented and connection\-less services whereas the Transport layer is responsible for establishing, using, and terminating a connection\. |

Click [here](Networking/networking-osi-layers_2/README.md) to see the answers.

## The ping command uses what protocol?

KSATs: K0615

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ICMP | HTTP | UDP | ARP |

Click [here](Networking/networking-ping_1/README.md) to see the answers.

## What type of document is authored by engineers and computer scientists in the form of a memorandum describing methods, behaviors, research, or innovations applicable to the working of the Internet and Internet\-connected systems?

KSATs: K0617

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| IEEE \- Instructions of Electrical and Electronics Engineers | RFC \- Request for Comments | W3C \- World Wide Web Comments | NGM \- Networking Guidance Memorandum |

Click [here](Networking/networking-rfc/README.md) to see the answers.

## This networking OSI layer establishes, manages and terminates connections between applications

KSATs: K0066

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Data Link \(layer 2\) | Network \(layer 3\) | Session \(layer 5\) | Physical \(layer 1\) |

Click [here](Networking/networking-session-layer/README.md) to see the answers.

## A TCP socket is different from a UDP socket because \_\_\_\.

KSATs: K0064, K0065, K0068, K0618

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| you have to specify both the IP address and the port, compared to UDP only requiring the IP address | the TCP socket works in a send and forget model | you have to connect to a remote node prior to sending a message | the socket doesn't have to be closed when communication is finished like you would do with a UDP socket |

Click [here](Networking/networking-socket-types_1/README.md) to see the answers.

## The client in socket programming must know which information?

KSATs: K0068, K0610

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| IP address of server | Port number | Both IP address of server and port number | None of the above |

Click [here](Networking/networking-socketserver_3/README.md) to see the answers.

## What is one reason for network subnetting?

KSATs: K0072

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| It helps relieve network congestion\. | It provides every organization an equal number of IP addresses\. | It allows IP octets above 255 | It reduces the need for Domain Name System \(DNS\) servers\. |

Click [here](Networking/networking-subnetting/README.md) to see the answers.

## What has CAM \(Content Addressable Memory\) tables that map a physical ports to MAC addresses

KSATs: K0089

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Hub | repeater | switch | DHCP server |

Click [here](Networking/networking-switches_1/README.md) to see the answers.

## In networking, which number type starts at the random number chosen during the handshake, is cumulative and is used to maintain accountability of all packets?

KSATs: K0064

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Sequence | Reserved | Nonce Sum | Acknowledgement |

Click [here](Networking/networking-syn-and-ack_1/README.md) to see the answers.

## The first item in a TCP header is:

KSATs: K0064

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| destination port | source port | sequence number | checksum |

Click [here](Networking/networking-tcp-header-and-flags_1/README.md) to see the answers.

## In networking, which TCP flag means there is no more data to send?

KSATs: K0064

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ACK | FIN | RST | SYN |

Click [here](Networking/networking-tcp-header-and-flags_2/README.md) to see the answers.

## The TCP process may not write and read data at the same speed, so \_\_\_\_\_\_ are needed for storage\.

KSATs: K0064

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| packets | buffers | segments | stacks |

Click [here](Networking/networking-tcp-process_1/README.md) to see the answers.

## Which of the following is correct about the UDP protocol?

KSATs: K0065

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Keeps track of lost packets | Adds sequence numbers | Checks packet arrival order | Faster than TCP |

Click [here](Networking/networking-udp/README.md) to see the answers.

## Compared to protocols like DNS or DHCP, UDP is different because \_\_\_\.

KSATs: K0065, K0066, K0613, K0616

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| it operates at the Transport layer of the Open Systems Interconnection \(OSI\) model | it operates at the Application layer of the OSI model | it can do the work of both DNS and DHCP | it operates at the Network layer of the OSI model |

Click [here](Networking/networking-udp_1/README.md) to see the answers.

## ***Python***

## Which of the following defines a Ctype\.

KSATs: K0029

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| a python module that enables the conversion of C functions into a Python functions\. | a python module that allows the declaration of specific C data types like int, and float etc\. | provides C compatible data types and allows function calls from DLLs but must have custom C extensions\. | provides C compatible data types and allows function calls from DLLs without having to write custom C extensions\. |

Click [here](Python/python-CTypes/README.md) to see the answers.

## Which of the following is an advantage of using object\-oriented programming?

KSATs: K0035, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| It makes the program run faster\. | It eliminates the need for functions\. | It provides a modular structure for programs\. | All the above are correct\. |

Click [here](Python/python-OO-advantages/README.md) to see the answers.

## In python when using inheritance how would you call the \_\_init\_\_\(\) function of a parent class inside a subclass \_\_init\_\_\(\) function

KSATs: K0030, K0036, K0037, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| ancestor\.\_\_init\_\_\(\) | super\(\)\.\_\_init\_\_\(\) | parent\(\)\.\_\_init\_\_\(\) | base\.\_\_init\_\_\(\) |

Click [here](Python/python-OO-super/README.md) to see the answers.

## What structural design pattern allows incompatible objects to collaborate?

KSATs: K0043, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Adapter | Cooperative | Structural | Compatible |

Click [here](Python/python-adapter-design/README.md) to see the answers.

```python
1 | x = 19/2
2 | value = x % 3 == 0
3 | 
4 | if value:
5 |     print (value, " is true")
6 | else:
7 |     print (value, " is false")
8 | 
```

## What is the output for the above Python code snippet in Python 3?

KSATs: K0690, K0715, K0735, K0754

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| value is false | False is false | value is true | True is true |

Click [here](Python/python-boolean_1/README.md) to see the answers.

## What structural design pattern divides business logic or a huge class into separate class hierarchies that can be developed independently?

KSATs: K0044, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Hierarchy | Logic | Bridge | Independent |

Click [here](Python/python-bridge-design/README.md) to see the answers.

```python
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age
```

## Given the above Python class, self\.age is an instance \_\_\_\_\_\_\_\_\_\_\_

KSATs: K0040, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| function | attribute | object | instance |

Click [here](Python/python-class-attributes/README.md) to see the answers.

## Which of the following is a valid function header definition for a Python Class' init function?

KSATs: K0030, K0032, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| def \_\_init\_\_\(self, name, age\): | def \_\_init\_\_\(name, age\): | def init\(self, name, age\) | def self\.init\(name, age\) |

Click [here](Python/python-class/README.md) to see the answers.

## In Object\-oriented programming, what function's purpose is to initialize one or more of an object's attributes when it's originally created?

KSATs: K0030, K0038, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Constructor | Initializer | Setter | Objectizer |

Click [here](Python/python-constructor/README.md) to see the answers.

## Which of the following is the proper way to access a value from a Python dictionary called car?

KSATs: K0014, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| car\.model | car\["model"\] | car\{model\} | car\.model\(\) |

Click [here](Python/python-data-types_1/README.md) to see the answers.

```python
1 | stuff = {"City":"Austin","State":"Texas","County":"Bexar"}
2 | 
3 | print (stuff["State"])
4 | 
```

## What is the output for the above Python code snippet?

KSATs: K0014, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Texas | A run\-time error occurs | Austin, Texas, Bexar | Austin |

Click [here](Python/python-data-types_2/README.md) to see the answers.

```python
1 | People = [
2 |     ["George","Jones"],
3 |     ["Bill","Johnson"],
4 |     ["Hank","Aaron"]
5 | ]
6 | 
```

## Given the above Python code snippet, how would you print out the word:  Aaron?

KSATs: K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| print\(people\[2\]\["Hank"\]\) | print\(people\[3\]\[2\]\) | print\(people\[2,1\]\) | print\(people\[2\]\[1\]\) |

Click [here](Python/python-data-types_3/README.md) to see the answers.

```python
1 | if var == 100   
2 |    print ("Value of expression is 100")
3 | 
```

## What is the syntax error in the above Python code snippet?

KSATs: K0690, K0754

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| indention must be at least 8 spaces | the : is missing after the if expression | the print statement is printing a hard\-coded string | all if expression require an else |

Click [here](Python/python-else_1/README.md) to see the answers.

## What design pattern solves the problem of creating product objects without specifying their concrete classes?

KSATs: K0041, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Concrete | Factory | Repeater | Product |

Click [here](Python/python-factory-design/README.md) to see the answers.

```python
1 |def reverse_file(filename):
2 |  fd = os.open(filename)
3 |  contents = fd.read()
4 |  os.remove(filename)
5 |  rfd = os.open('reverse.txt', 'w')
6 |  rfd.write(contents[::-1])
7 |  with open(filename) as fd:
8 |    return fd.read()

```

## In the above image, which line of code contains an issue preventing the function from reading and then deleting a file and writing its contents in reverse to a new file?

KSATs: K0690, K0741, K0742, K0743

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| line 3 | line 4 | line 5 | line 6 |

Click [here](Python/python-file-making/README.md) to see the answers.

```python
1 | def do_files_1(filename):
2 |    fd = os.open(filename)
3 |    return fd.read()
4 |
5 |
6 | def do_files_2(filename):
7 |    with open(filename) as fd:
8 |       return fd.read()
9 | 
```

## What is the difference between the above functions?

KSATs: K0690, K0741

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| There is no difference between the two functions | The first functions only returns a file descriptor | The second function closes the file | The first function requires the absolute path while the second requires the relative path |

Click [here](Python/python-file-opening/README.md) to see the answers.

## Which line of code would you use to access the 16th byte in a file so that it can be overwritten?

KSATs: K0690, K0745

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| fd\[16\] | fd\.read\(16\) | fd\.move\(16\) | fd\.seek\(16\) |

Click [here](Python/python-file-searching/README.md) to see the answers.

## How would you find the size of a file in bytes?

KSATs: K0690, K0744

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| os\.len\('example\.txt'\) | os\.size\('example\.txt'\) | os\.sizeof\('example\.txt'\) | os\.stat\('example\.txt'\)\.st\_size |

Click [here](Python/python-file-stats/README.md) to see the answers.

## Given a Python list called ages, which is the proper way to iterate through each item in the list\.

KSATs: K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| for int a in ages: | for a in ages: | for range\(a\) in ages: | for range\(ages\): |

Click [here](Python/python-flow-control_1/README.md) to see the answers.

```python
1 | values = [2, 3, 4, 6, 7, 9, 12, 18]
2 | 
3 | for i in range(2, 7):
4 |     if values[i] % 2 == 0 and values[i] % 3 == 0:
5 |         print (values[i])
6 | 
```

## What is the output for the above Python code snippet?

KSATs: K0015, K0690, K0713, K0714, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 4<br>6<br>9<br>12 | 6<br>12 | 4<br>12 | 3<br>6<br>7 |

Click [here](Python/python-flow-control_2/README.md) to see the answers.

```python
1 | values = []
2 | 
3 | for i in range(10):
4 |     values.append(i * 2)  
5 | for i in range(3, len(values)-3):
6 |     print (values[i])
7 | 
```

## What is the output for the above Python code snippet?

KSATs: K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 6<br>8<br>10<br>12 | 6<br>8<br>10<br>12<br>14 | 4<br>6<br>8<br>10<br>12 | 3<br>4<br>5<br>6 |

Click [here](Python/python-flow-control_3/README.md) to see the answers.

```python
1 | values = [3, 5, 7, 9, 11, 13, 15]
2 | 
3 | for i in range(2, len(values), 2):
4 |     print (values[i])
5 | 
```

## What is the output for the above Python code snippet?

KSATs: K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 5<br>7 | 7<br>9<br>11<br>13<br>15 | 7<br>11 | 7<br>11<br>15 |

Click [here](Python/python-flow-control_4/README.md) to see the answers.

## Which of the following is the proper way to declare a function header in Python?

KSATs: K0009, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| def create\_stuff\(\): | create\_stuff\(\): | create\_stuff: | func create\_stuff\(\) |

Click [here](Python/python-functions_1/README.md) to see the answers.

## What is a difference between the yield keyword and the return keyword

KSATs: K0027, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| yield temporarily blocks the program from executing | yield exits the program entirely | yield transfers interpreter control to the next thread in a threadpool | yield keeps function state after it returns a value |

Click [here](Python/python-generator-function/README.md) to see the answers.

## In Object\-Oriented programming, functions designed to change an object's attribute is known as a \_\_\_\_\_\_\_\_\_\_\_ function and a function that retrieves an object's attribute is known as a \_\_\_\_\_\_\_\_\_ function?

KSATs: K0030, K0039, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| modifier, viewer | setter, getter | updater, retriever | changer, viewer |

Click [here](Python/python-getter-setter/README.md) to see the answers.


---

### Begin PYTHON-GROUP_1 Questions

Please refer to the code snippet below for the following 9 question(s).

```python
1  | def getAverage(GoT):
2  |    tot = 0
3  |    count = 0
4  |    for rank in GoT.values():
5  |       tot += rank
6  |       count += 1
7  | 
8  |    return int(tot/count)
9  | 
10 | names = ['Cersei','Arya','Jon','Denerys','Tyrion','Jon','Sam','Arya'] 
11 |    
12 | peeps = {'Jon':16,'Denerys':7,'Tyrion':15,'Cersei':2,'Arya':20}
13 | 
14 | print(peeps[names[0]])
15 | 
16 | peeps['Tyrion'] = 18
17 | 
18 | avg = getAverage(peeps)
19 | 
20 | castNames = {'Arya'}
21 | 
22 | for name in names:
23 |    castNames.add(name)
24 | 
25 | print(names[len(names)])
26 | 
```

## Which line contains a type cast?

KSATs: K0008, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1 | 8 | 18 | 20 |

Click [here](Python/python-group_1/cast_1/README.md) to see the answers.

## What is the data type of avg?

KSATs: K0008, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Float | Integer | Complex | Double |

Click [here](Python/python-group_1/data-types_4/README.md) to see the answers.

## On line 14, what is printed?

KSATs: K0014, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 2 | Jon | Cersei | 16 |

Click [here](Python/python-group_1/dict-list_1/README.md) to see the answers.

## What is occurring on line 16?

KSATs: K0014, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The value associated with 'Tyrion' is changed to 18 | An additional key/value pair of 'Tyrion' and 18 is added | The value associated with 'Tyrion' is increased by 18 | None of the above |

Click [here](Python/python-group_1/dict_1/README.md) to see the answers.

## What type of container is peeps?

KSATs: K0017, K0016, K0014, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| List | Set | Dictionary | Tuple |

Click [here](Python/python-group_1/dict_2/README.md) to see the answers.

## What value does the getAverage function return?

KSATs: K0007, K0008, K0009, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 12 | 6 | 14 | 2 |

Click [here](Python/python-group_1/functions_2/README.md) to see the answers.

## What is the result of line 25 executing?

KSATs: K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Prints the string Arya | Prints Nothing | List index out of range error | Prints all strings in the list |

Click [here](Python/python-group_1/lists_5/README.md) to see the answers.

## In the image above, what type of container is names?

KSATs: K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| List | Array | String | Set |

Click [here](Python/python-group_1/lists_7/README.md) to see the answers.

## What type of container is castNames?

KSATs: K0017, K0016, K0014, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| List | Set | Dictionary | Tuple |

Click [here](Python/python-group_1/set_1/README.md) to see the answers.


### End PYTHON-GROUP_1 Questions

---


---

### Begin PYTHON-GROUP_2 Questions

Please refer to the code snippet below for the following 7 question(s).

```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## What is self\.name in the Person class?

KSATs: K0040, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Attribute | Constructor | Class | Function |

Click [here](Python/python-group_2/attribute/README.md) to see the answers.

## What is occurring on line 2?

KSATs: K0038

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| A class is created | A constructor function is being overloaded | The initialization function of a constructor is being defined | The self actualization function of the object is called |

Click [here](Python/python-group_2/constructor/README.md) to see the answers.

## What is being implemented on line 8?

KSATs: K0030, K0036, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Polymorphism | Inheritance | A list | Overloading |

Click [here](Python/python-group_2/inheritance/README.md) to see the answers.

## What is 'peep' in line 18 of the above code snippet?

KSATs: K0033, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| A String | An Object | A Class | A Set |

Click [here](Python/python-group_2/object/README.md) to see the answers.

## Which line is implementing polymorphism?

KSATs: K0031, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 8 | 10 | 13 | 19 |

Click [here](Python/python-group_2/polymorphism/README.md) to see the answers.

## What is being created in line 16?

KSATs: K0017, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| A list | An array | A set | A tuple |

Click [here](Python/python-group_2/set/README.md) to see the answers.

## What keyword in the subclass is used to call a superclass function?

KSATs: K0037, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Person | self | super | self\.name |

Click [here](Python/python-group_2/super/README.md) to see the answers.


### End PYTHON-GROUP_2 Questions

---

## In Object\-oriented programming, a sub class is created using a super class via \_\_\_\_\_\_\_\_\_\_\_\_\_\_?

KSATs: K0036, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Inheritance | Extending | Overriding | Casting |

Click [here](Python/python-inheritance/README.md) to see the answers.

```python
1 | mytuple = ('apple', 'banana', 'cherry')
2 | 
```

## Given the above Python Tuple declaration, which of the following is a valid iterator declaration?

KSATs: K0026, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| myit = iterable\(mytuple\) | mytuple\.iter\(\) | mytuple = iterator\(mytuple\) | myit = iter\(mytuple\) |

Click [here](Python/python-iterators/README.md) to see the answers.

## which of the following is not a difference between lambda functions and normal functions

KSATs: K0028, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| lambda functions can only have one expression | lambda functions can not use the return keyword | lambda functions can not have names | lambda functions can access variables from the function that called it |

Click [here](Python/python-lambda-function/README.md) to see the answers.

## Which of the following is NOT a term associated with a linked list?

KSATs: K0056, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Index | Head | Node | Next |

Click [here](Python/python-linked-list/README.md) to see the answers.

```python
1 | S = [['him', 'sell'], [90, 28, 43]]   
2 |    print(S[0][1][1])
3 | 
```

## What is the output for the above Python snippet?

KSATs: K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| e | i | 90 | h |

Click [here](Python/python-list-of-list/README.md) to see the answers.

```python
1 | names1 = ['Amir', 'Bala', 'Chales']
2 | 
3 | if 'amir' in names1:
4 |     print (1)
5 | else:
6 |     print (2)
7 | 
```

## What will be the output of the above Python code snippet?

KSATs: K0015, K0690, K0735, K0754

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Amir | 1 | 2 | Bala |

Click [here](Python/python-lists_1/README.md) to see the answers.

```python
1 | values = [[3, 4, 5, 1], [33, 6, 1, 2]]
2 | 
3 | for row in values:
4 |     row.sort()
5 |     for element in row:
6 |         print(element, end = "  " )
7 | 
```

## What will be the output of the above Python code snippet?

KSATs: K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 3 4 5 1 33 6 1 2 | 3 4 5 1 33 6 1 2 | 1 1 2 3  4 5 6 33 | 1 3 4 5 1 2 6 33 |

Click [here](Python/python-lists_2/README.md) to see the answers.

```python
1 | list1 = [1, 2, 3, 4]
2 | list2 = [3, 4, 7, 8]
3 | 
4 | print (len(list1 + list2))
5 | 
```

## What will be the output of the above Python code snippet?

KSATs: K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 2 | 4 | 6 | 8 |

Click [here](Python/python-lists_3/README.md) to see the answers.

## In Python, given a list called myList, which places data into the current last element of the list?

KSATs: K0015, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| myList\(end\) = 5 | myList\[myList\.end\] = 5 | myList\[len\(myList\)\-1\] = 5 | len\(myList\) = 5 |

Click [here](Python/python-lists_4/README.md) to see the answers.

```python
1 | a = ['Python', 'is', 'not', 'tough']
2 | for i in range(len(a)):
3 |      print (i, a[i], end=" ")
4 | 
```

## What is the output for the above Python code snippet?

KSATs: K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 1 Python 2 is 3 not 4 tough | Python is not tough | 1 Python 2 is 3 not | 0 Python 1 is 2 not 3 tough |

Click [here](Python/python-lists_6/README.md) to see the answers.

## The Python container used to store key\-pair values is a:

KSATs: K0016, K0014, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| List | Dictionary | Map | Tuple |

Click [here](Python/python-mapping-&-other-types_1/README.md) to see the answers.

## Which of the following is a valid declaration of a dictionary in Python?

KSATs: K0014, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| dict=\{'Country':'India','Capital':'Delhi','PM':'Modi'\} | dict=\['Country':'India','Capital':'Delhi','PM':'Modi'\] | dict=\{'Country'\-'India','Capital'\-'Delhi','PM'\-'Modi'\} | dict=\{'Country','India','Capital','Delhi','PM','Modi'\} |

Click [here](Python/python-mapping_1/README.md) to see the answers.

## In Python, what object is unordered, unique \(no duplicate values\) and immutable\.

KSATs: K0016, K0018, K0014, K0019, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| list | tuple | set | dictionary |

Click [here](Python/python-mapping_2/README.md) to see the answers.

## In Python, the \_\_\_\_\_\_\_\_\_\_ statement is used to utilize other Python modules\.

KSATs: K0013, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| import | include | define | module |

Click [here](Python/python-modules_1/README.md) to see the answers.

```python
1 | def findLowHigh(lst):
 2 |     lst.sort()
 3 |     return lst[2], lst[3]
 4 |     
 5 | a, b = findLowHigh([8,9,2,6,12,2])
 )
```

## In the above python snippet, what is the values of a and b when complete?

KSATs: K0015, K0690, K0736, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| a is 2 and b is 3\. | a is 6 and b is 8\. | a is 2 and b is 6\. | a is 2 and b is 12\. |

Click [here](Python/python-multiple-values/README.md) to see the answers.

## In python, which of the following object types are mutable\.

KSATs: K0016, K0018, K0019, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| list | tuple | int | string |

Click [here](Python/python-mutable-objects/README.md) to see the answers.

```python
1 | numbers = [2, 3, 4, 5, 6]
2 | for x in numbers:
3 |     if x % 2 == 0:
4 |         print (x*x, end = " ")
5 |     else:
6 |         print (x, end = " ")
7 | 
```

## What is the output for the above Python code snippet?

KSATs: K0015, K0690, K0754

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 4 3 9 5 36 | 4 3 16 5 36 7 | 4 3 16 5 36 | 2 3 16 5 36 |

Click [here](Python/python-numbers_1/README.md) to see the answers.

```python
1 | class Dog:
2 |     def __init__(self, name):
3 |         self.name = name 
4 |  
5 | myDog = Dog("pepe")
6 | 
```

## In the above python snippet, what is myDog?

KSATs: K0033, K0034, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| object | class | attribute | constructor |

Click [here](Python/python-object-type/README.md) to see the answers.

## Which of the following is NOT a keyword used in Python exception handling?

KSATs: K0690, K0710

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| finally | break | try | except |

Click [here](Python/python-oop_1/README.md) to see the answers.

```python
1 | x = 4
2 | y = 2
3 | print(x == 4 and y == 2)

```

## In the above Python snippet, what is printed?

KSATs: K0035, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 4 2 | False | True | x == 4 and y == 2 |

Click [here](Python/python-operators_1/README.md) to see the answers.

## Which is the correct operator to use an exponent?

KSATs: K0690, K0716, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| x^y | x\*\*y | x^^y | x\(pow\)y |

Click [here](Python/python-operators_2/README.md) to see the answers.

## What is the result of 3 % 5 in Python 3?

KSATs: K0008, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 3 | 0 | 2 | 5 |

Click [here](Python/python-operators_3/README.md) to see the answers.

## Which of the following is a Python Enhancement Proposal \(PEP8\) coding style for Python?

KSATs: K0021, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Tabs are the preferred indentation method | Spaces are the preferred indentation method | Tab width or spaces for indentation should be six spaces wide | Use tab for first level indentation, and spaces for any subsequent indentation |

Click [here](Python/python-pep8/README.md) to see the answers.

## Which of the following is the proper command to install a python module?

KSATs: K0690, K0733

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| pip install superModule | python install superModule | python\.install\(\) superModule | install\.py superModule |

Click [here](Python/python-pip-install/README.md) to see the answers.

## What python module automatically generates documentation from Python modules?

KSATs: K0022, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| document | pyDoc | modDocs | pyComments |

Click [here](Python/python-pydocs/README.md) to see the answers.

```python
1 | def factorial(n):
 2 |     if n == 1:
 3 |         return 1
 4 |     else:
 5 |         return n * factorial(n-1)
```

## What is true about the above Python function definition?

KSATs: K0690, K0737

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| It is a recursive function\. | It is a looping function\. | It should only have one return statement\. | It will always return 1 regardless of n's value\. |

Click [here](Python/python-recursion/README.md) to see the answers.

## What happens when a return statement is executed in a Python function?

KSATs: K0007, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| It returns execution to the top of the function\. | It returns execution to the beginning of the script\. | It terminates the function's execution\. | It returns the function's address to the caller\. |

Click [here](Python/python-return-statement/README.md) to see the answers.

```python
 1   |  def myfunction(val):
 2   |      i = val
 3   |      i += 10
 4   |
 5   |  i = 7
 6   |
 7   |  myfunction(0)
 8   |
 9   |  i += 2
 10  |  
 11  |  print(i)
```

## What will print on line 11 of the Python snippet above?

KSATs: K0011, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 9 | 12 | 19 | 2 |

Click [here](Python/python-scope/README.md) to see the answers.

```python
1   |  dogs = {'Yorkie','Terrier','Doberman','Yorkie'}

```

## What is true about the container created in the Python snippet above?

KSATs: K0017, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| It is a List of size 4 | It is a Set of size 4 | It is a Set of size 3 | It is a Tuple of size 4 |

Click [here](Python/python-set-2/README.md) to see the answers.

## In python, which design pattern restricts the instantiation of a class to one object?

KSATs: K0042, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Bridge | Individual | Singleton | Unique |

Click [here](Python/python-singleton/README.md) to see the answers.

## Which built\-in function reads a line of text from standard input, which by default, comes from the keyboard?

KSATs: K0024, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| read | input | scan | A or B |

Click [here](Python/python-std-lib_1/README.md) to see the answers.

```python
1 | sentence = "The light-brown fox."
2 | print (sentence[9:])
3 | 
```

## What is the output for the above Python code snippet?

KSATs: K0690, K0711, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| brown fox\. | b | \- | \-brown fox\. |

Click [here](Python/python-strings_1/README.md) to see the answers.

```python
1 | sentence = "The light-brown fox."
2 | print (sentence[len(sentence)-2])
3 | 
```

## What is the output for the above Python code snippet?

KSATs: K0690, K0711, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| x | \. | o | fox |

Click [here](Python/python-strings_2/README.md) to see the answers.

```python
1 | myList = ["Jim", "Bob", "Matt", "Tom"]
2 | 
```

## Which Python string function would you use to combine a list of strings into one string with each word separated by a space?

KSATs: K0015, K0690, K0711, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| " "\.append\(myList\) | myList\.append\(" "\) | " "\.join\(myList\) | " "\.collect\(myList\) |

Click [here](Python/python-strings_4/README.md) to see the answers.

```python
1 | name = "Jimmy Crack Corn"
2 | 
3 | print (name[:-5])
4 |
```

## What is the output for the above Python code snippet?

KSATs: K0690, K0711, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| invalid syntax | Jimmy Crack Corn | Jimmy Crack | Crack Corn |

Click [here](Python/python-strings_5/README.md) to see the answers.

```python
1 | str = 'pdf csv json'
2 | print(str.split())
3 | 
```

## What is the output for the above Python code snippet?

KSATs: K0015, K0690, K0711, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| \['pdf', 'csv', 'json'\] | \[pdf, csv, json\] | pdf, csv, json | pdf csv json |

Click [here](Python/python-strings_6/README.md) to see the answers.

```python
1 | age_book = {"Sam" : 19, "Timmy" : 24, "Beth" : 22}
2 | 
3 | ages = get_ages(age_book)
4 | age_collection = [ages[0], ages[1], ages[2]]
5 | 
6 | def get_ages(a):
7 |     return a["Sam"], a["Timmy"], a["Beth"]
8 | 
```

## In the above Python snippet, which is a tuple?

KSATs: K0007, K0012, K0016, K0014, K0015, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| age\_book | ages | age\_collection | Both ages and age\_collection |

Click [here](Python/python-tuple_1/README.md) to see the answers.

## The difference between a tuple and a list in Python is:

KSATs: K0016, K0018, K0019, K0015, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| a tuple is dynamic while a list is not | tuples can only contain numbers while lists can contain any type | a tuple, once created, cannot be changed | a list is faster to access than a tuple |

Click [here](Python/python-tuple_2/README.md) to see the answers.

```python
 1 | x= 10
 2 |
 3 | y = 8.76
 4 |
 5 | y = int(y)
 6 |
 7 | z = y == 9 and x == 10

```

## In the above Python snippet, what is the value of z?

KSATs: K0008, K0690, K0712, K0715, K0714, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| 9 | 10 | False | True |

Click [here](Python/python-type-cast-and-logical-op/README.md) to see the answers.

```python
1 | def printStuff(stuff):
2 |     print (stuff)
3 | 
```

## Given the Python function defined above, which of the following are valid calls to the function?

KSATs: K0008, K0010, K0009, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| printStuff\(34\) | printStuff\("Hello"\) | printStuff\(True\) | all the above are valid calls |

Click [here](Python/python-variables_1/README.md) to see the answers.

```python
1 | x = 10
2 | x += 1
3 | 
```

## On the second line of the above Python snippet, x is now 11\.  What else is occurring?

KSATs: K0008, K0018, K0019, K0690, K0735

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| The original int object was changed from 10 to 11 because it's mutable\. | A new int object is created because int objects are immutable | A new int object is created because int objects are static | The original int object was changed from 10 to 11 because it's dynamic\. |

Click [here](Python/python-variables_2/README.md) to see the answers.

## Which of the following is a valid Python 3\.x print statement?

KSATs: K0023, K0690

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| print "hello" | print\("hello"\) | print\."hello" | a and b are both correct |

Click [here](Python/python-version/README.md) to see the answers.

## ***Software Engineering Fundamentals***

## Which of the following supports good modular design practices?

KSATs: K0002

| A | B | C | D |
| :--- | :--- | :--- | :--- |
| Creating meaningful variable names\. | Creating functions with two parameters or fewer\. | Creating reusable functions that provide a unique service\. | If there are more than five sequential statements in a program, a function should be created for it\. |

Click [here](Software_Engineering_Fundamentals/SEF-modular-design/README.md) to see the answers.

