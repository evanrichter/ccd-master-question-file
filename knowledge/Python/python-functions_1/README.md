## Which of the following is the proper way to declare a function header in Python?

KSATs: K0009, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***def create\_stuff\(\):*** | create\_stuff\(\): | create\_stuff: | func create\_stuff\(\) |


Feedback:

- A) Correct: This is the proper way to declare a Python function\.
- B) Incorrect: A proper Python function declaration requires the def keyword before the function prototype\.
- C) Incorrect: Without the colon it would be a variable\. To be a function header, it needs to be preceded by the keyword def and \(\) needs to be inserted between the end of create\_stuff and the colon\.
- D) Incorrect: func is not a valid keyword in Python and should be changed with def\. Also, a trailing colon is required\.

[**<- Back To README**](../../README.md)

