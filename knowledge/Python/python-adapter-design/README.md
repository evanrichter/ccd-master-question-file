## What structural design pattern allows incompatible objects to collaborate?

KSATs: K0043, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Adapter*** | Cooperative | Structural | Compatible |


Feedback:

- Adapter design pattern is a structural design pattern, which allows incompatible objects to collaborate

[**<- Back To README**](../../README.md)

