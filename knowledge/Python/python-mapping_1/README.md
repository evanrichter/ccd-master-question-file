## Which of the following is a valid declaration of a dictionary in Python?

KSATs: K0014, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***dict=\{'Country':'India','Capital':'Delhi','PM':'Modi'\}*** | dict=\['Country':'India','Capital':'Delhi','PM':'Modi'\] | dict=\{'Country'\-'India','Capital'\-'Delhi','PM'\-'Modi'\} | dict=\{'Country','India','Capital','Delhi','PM','Modi'\} |


Feedback:

- A) Correct: The valid declaration of a Python dictionary object is dict = \{\}, for a blank dictionary or dict = \{key:value, \.\.\. key\_n:value\_n\}\.
- B) Incorrect: This tries to use a list as a dictionary, since square brackets create list objects, and is invalid syntax\.
- C) Incorrect: You must use a colon as the key:value separator, not dash; this is invalid syntax\.
- D) Incorrect: This creates a set, not a dictionary\.

[**<- Back To README**](../../README.md)

