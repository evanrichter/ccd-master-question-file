## In python when using inheritance how would you call the \_\_init\_\_\(\) function of a parent class inside a subclass \_\_init\_\_\(\) function

KSATs: K0030, K0036, K0037, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| ancestor\.\_\_init\_\_\(\) | ***super\(\)\.\_\_init\_\_\(\)*** | parent\(\)\.\_\_init\_\_\(\) | base\.\_\_init\_\_\(\) |


Feedback:

- the super keyword is used to reference an item in the inherited base class from the sub\-class in python\.

[**<- Back To README**](../../README.md)

