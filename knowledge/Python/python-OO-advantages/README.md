## Which of the following is an advantage of using object\-oriented programming?

KSATs: K0035, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| It makes the program run faster\. | It eliminates the need for functions\. | ***It provides a modular structure for programs\.*** | All the above are correct\. |


Feedback:

- Object\-oriented programming facilitates modular structures and actually facilitates code reuse, it has no effect on increasing processing time\.

[**<- Back To README**](../../README.md)

