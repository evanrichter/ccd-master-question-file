## Which of the following is a Python Enhancement Proposal \(PEP8\) coding style for Python?

KSATs: K0021, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Tabs are the preferred indentation method | ***Spaces are the preferred indentation method*** | Tab width or spaces for indentation should be six spaces wide | Use tab for first level indentation, and spaces for any subsequent indentation |


Feedback:

- Spaces are the preferred indentation according to PEP8 guidance

[**<- Back To README**](../../README.md)

