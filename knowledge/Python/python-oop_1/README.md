## Which of the following is NOT a keyword used in Python exception handling?

KSATs: K0690, K0710

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| finally | ***break*** | try | except |


Feedback:

- Try, except, and finally are all exception handling keywords; break is not an exception handling keyword but does allow breaking out of loops\.

[**<- Back To README**](../../README.md)

