## Which of the following is a valid Python 3\.x print statement?

KSATs: K0023, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| print "hello" | ***print\("hello"\)*** | print\."hello" | a and b are both correct |


Feedback:

- Python 2\.x does not require parentheses for print, but it can be used, it is actually a special statement and not a function\. It is required in python 3\.x to use parentheses\.

[**<- Back To README**](../../README.md)

