## What design pattern solves the problem of creating product objects without specifying their concrete classes?

KSATs: K0041, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Concrete | ***Factory*** | Repeater | Product |


Feedback:

- Factory design pattern solves the problem of creating product objects without specifying their concrete classes

[**<- Back To README**](../../README.md)

