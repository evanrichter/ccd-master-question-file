## In Object\-oriented programming, what function's purpose is to initialize one or more of an object's attributes when it's originally created?

KSATs: K0030, K0038, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Constructor*** | Initializer | Setter | Objectizer |


Feedback:

- The constructor is executed when an object is created and is designed to initialize an object's attributes upon creation\. Setter functions can only be used after the object is created\.

[**<- Back To README**](../../README.md)

