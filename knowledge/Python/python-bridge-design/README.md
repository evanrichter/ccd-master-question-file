## What structural design pattern divides business logic or a huge class into separate class hierarchies that can be developed independently?

KSATs: K0044, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Hierarchy | Logic | ***Bridge*** | Independent |


Feedback:

- Bridge design pattern is a structural design pattern that divides business logic or a huge class into separate class hierarchies that can be developed independently

[**<- Back To README**](../../README.md)

