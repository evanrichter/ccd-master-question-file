## Which of the following is the proper command to install a python module?

KSATs: K0690, K0733

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***pip install superModule*** | python install superModule | python\.install\(\) superModule | install\.py superModule |


Feedback:

- pip \(Python Package Installer\) is the command used to install a module\.

[**<- Back To README**](../../README.md)

