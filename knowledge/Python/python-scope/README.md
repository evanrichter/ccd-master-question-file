```python
 1   |  def myfunction(val):
 2   |      i = val
 3   |      i += 10
 4   |
 5   |  i = 7
 6   |
 7   |  myfunction(0)
 8   |
 9   |  i += 2
 10  |  
 11  |  print(i)
```

## What will print on line 11 of the Python snippet above?

KSATs: K0011, K0690, K0735

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***9*** | 12 | 19 | 2 |


Feedback:

- The i variable inside the function is out of scope from the i variable in the main script\.  They are two different variables\. Changes to one will not affect the other\.

[**<- Back To README**](../../README.md)

