```python
1 | stuff = {"City":"Austin","State":"Texas","County":"Bexar"}
2 | 
3 | print (stuff["State"])
4 | 
```

## What is the output for the above Python code snippet?

KSATs: K0014, K0690, K0735

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Texas*** | A run\-time error occurs | Austin, Texas, Bexar | Austin |


Feedback:

- Since stuff is a Python dictionary, stuff\["State"\] accesses the value that the key 'State' points to, which is 'Texas'\.

[**<- Back To README**](../../README.md)

