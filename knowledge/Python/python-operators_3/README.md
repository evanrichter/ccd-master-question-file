## What is the result of 3 % 5 in Python 3?

KSATs: K0008, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***3*** | 0 | 2 | 5 |


Feedback:

- Since the % character represents the modulus arithmetic operation, the answer is the remainder of 3 / 5, which is 3\.

[**<- Back To README**](../../README.md)

