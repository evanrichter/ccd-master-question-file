## What python module automatically generates documentation from Python modules?

KSATs: K0022, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| document | ***pyDoc*** | modDocs | pyComments |


Feedback:

- pyDocs is a module that will generate documents based off comments embedded in your code\.

[**<- Back To README**](../../README.md)

