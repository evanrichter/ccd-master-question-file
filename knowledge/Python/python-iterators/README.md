```python
1 | mytuple = ('apple', 'banana', 'cherry')
2 | 
```

## Given the above Python Tuple declaration, which of the following is a valid iterator declaration?

KSATs: K0026, K0690

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| myit = iterable\(mytuple\) | mytuple\.iter\(\) | mytuple = iterator\(mytuple\) | ***myit = iter\(mytuple\)*** |


Feedback:

- A) a is incorrect because the there is not a built in iterable function
- B) b is incorrect because mytuple does not have an iter method
- C) c is incorrect because the there is not a built in iterator function
- D) d is correct because iter is a built in function to create an iterator of iterable

[**<- Back To README**](../../README.md)

