## In Python, the \_\_\_\_\_\_\_\_\_\_ statement is used to utilize other Python modules\.

KSATs: K0013, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***import*** | include | define | module |


Feedback:

- A) Correct: the import statement brings in the code from the requested module
- B) Incorrect: this statement does not work in Python but would in C, C\+\+, etc\.
- C) Incorrect: the define statement is not used in Python but is used in C, C\+\+, etc\.
- D) Incorrect: this explains a python file that performs certain features; it isn't used to import modules

[**<- Back To README**](../../README.md)

