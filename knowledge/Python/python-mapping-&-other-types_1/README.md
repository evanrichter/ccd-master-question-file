## The Python container used to store key\-pair values is a:

KSATs: K0016, K0014, K0015, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| List | ***Dictionary*** | Map | Tuple |


Feedback:

- A) Incorrect: A list stores values only\.
- B) Correct: A dictionary stores key:value pairs\.
- C) Incorrect: A map is not a valid container in Python\.
- D) Incorrect: A tuple stores unique values only\.

[**<- Back To README**](../../README.md)

