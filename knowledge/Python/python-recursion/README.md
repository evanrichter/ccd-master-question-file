```python
1 | def factorial(n):
 2 |     if n == 1:
 3 |         return 1
 4 |     else:
 5 |         return n * factorial(n-1)
```

## What is true about the above Python function definition?

KSATs: K0690, K0737

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***It is a recursive function\.*** | It is a looping function\. | It should only have one return statement\. | It will always return 1 regardless of n's value\. |


Feedback:

- A recursive function is a function that calls itself as illustrated on line \#5\.

[**<- Back To README**](../../README.md)

