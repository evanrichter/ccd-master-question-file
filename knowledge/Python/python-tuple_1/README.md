```python
1 | age_book = {"Sam" : 19, "Timmy" : 24, "Beth" : 22}
2 | 
3 | ages = get_ages(age_book)
4 | age_collection = [ages[0], ages[1], ages[2]]
5 | 
6 | def get_ages(a):
7 |     return a["Sam"], a["Timmy"], a["Beth"]
8 | 
```

## In the above Python snippet, which is a tuple?

KSATs: K0007, K0012, K0016, K0014, K0015, K0690, K0735

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| age\_book | ***ages*** | age\_collection | Both ages and age\_collection |


Feedback:

- A) Incorrect: This is a dictionary
- B) Correct: get\_ages returns a tuple containing all of the return values
- C) Incorrect: This is a list
- D) Incorrect: ages is a tuple but age\_collection is a list

[**<- Back To README**](../../README.md)

