```python
1 | sentence = "The light-brown fox."
2 | print (sentence[len(sentence)-2])
3 | 
```

## What is the output for the above Python code snippet?

KSATs: K0690, K0711, K0735

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***x*** | \. | o | fox |


Feedback:

- Since 'len\(sentence\)\-2 is inside the square brackets of sentence\[\], the character in that specific position is accessed and printed\. Starting with \. as the first character, this means x will be printed\.

[**<- Back To README**](../../README.md)

