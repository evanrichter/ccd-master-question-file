```python
1 | names1 = ['Amir', 'Bala', 'Chales']
2 | 
3 | if 'amir' in names1:
4 |     print (1)
5 | else:
6 |     print (2)
7 | 
```

## What will be the output of the above Python code snippet?

KSATs: K0015, K0690, K0735, K0754

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Amir | 1 | ***2*** | Bala |


Feedback:

- Looking at Line 3, the conditional compares 'amir' with anything in the names1 list\. Since 'Amir' is not the same as 'amir', the condition becomes False and 2 is printed\.

[**<- Back To README**](../../README.md)

