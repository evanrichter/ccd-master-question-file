```python
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age
```

## Given the above Python class, self\.age is an instance \_\_\_\_\_\_\_\_\_\_\_

KSATs: K0040, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| function | ***attribute*** | object | instance |


Feedback:

- a variable declared with a prepended self\. indicates a Instance attribute

[**<- Back To README**](../../README.md)

