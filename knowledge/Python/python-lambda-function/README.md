## which of the following is not a difference between lambda functions and normal functions

KSATs: K0028, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| lambda functions can only have one expression | lambda functions can not use the return keyword | ***lambda functions can not have names*** | lambda functions can access variables from the function that called it |


Feedback:

- While lambda functions do not require an identifier, they can still be assigned a name and then called like a normal function\.

[**<- Back To README**](../../README.md)

