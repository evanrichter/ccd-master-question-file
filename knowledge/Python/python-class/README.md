## Which of the following is a valid function header definition for a Python Class' init function?

KSATs: K0030, K0032, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***def \_\_init\_\_\(self, name, age\):*** | def \_\_init\_\_\(name, age\): | def init\(self, name, age\) | def self\.init\(name, age\) |


Feedback:

- \_\_init\_\_ with self as a parameter is the valid declaration

[**<- Back To README**](../../README.md)

