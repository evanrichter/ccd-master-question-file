```python
1 | def findLowHigh(lst):
 2 |     lst.sort()
 3 |     return lst[2], lst[3]
 4 |     
 5 | a, b = findLowHigh([8,9,2,6,12,2])
 )
```

## In the above python snippet, what is the values of a and b when complete?

KSATs: K0015, K0690, K0736, K0735

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| a is 2 and b is 3\. | ***a is 6 and b is 8\.*** | a is 2 and b is 6\. | a is 2 and b is 12\. |


Feedback:

- The function receives a list, then sorts the list\. The function then returns two values at index 2 and 3 of the list\. The first value will be returned to variable a, the second to variable b\.

[**<- Back To README**](../../README.md)

