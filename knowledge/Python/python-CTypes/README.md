## Which of the following defines a Ctype\.

KSATs: K0029

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| a python module that enables the conversion of C functions into a Python functions\. | a python module that allows the declaration of specific C data types like int, and float etc\. | provides C compatible data types and allows function calls from DLLs but must have custom C extensions\. | ***provides C compatible data types and allows function calls from DLLs without having to write custom C extensions\.*** |


Feedback:

- CTypes provide C compatible data types and allow function calls from DLLs or shared libraries without having to write custom C extensions for every operation\. So functionality of a C library can be accessed from the safety of the Python Standard Library\.

[**<- Back To README**](../../README.md)

