```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## What is being created in line 16?

KSATs: K0017, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| A list | An array | ***A set*** | A tuple |


Feedback:

- A) Incorrect: A list is created using square \[\] brackets
- B) Incorrect: Arrays are a C construct, not python
- C) Correct: A set is created using curly \{\} braces
- D) Incorrect: A tuple is created using parenthesis \(\)

[**<- Back To README**](../../../README.md)

