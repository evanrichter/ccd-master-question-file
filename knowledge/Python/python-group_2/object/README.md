```python
1  |   class Person:
2  |     def __init__(self, name):
3  |         self.name = name
4  |
5  |     def print(self):
6  |         print(self.name)
7  |
8  |   class Airman(Person):
9  |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
20 | 
```

## What is 'peep' in line 18 of the above code snippet?

KSATs: K0033, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| A String | ***An Object*** | A Class | A Set |


Feedback:

- peep represents each item in the people set defined earlier which is a set of objects

[**<- Back To README**](../../../README.md)

