```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## Which line is implementing polymorphism?

KSATs: K0031, K0690

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| 8 | 10 | 13 | ***19*** |


Feedback:

- A) Incorrect: This is implementing a sub class
- B) Incorrect: This is a call the Person constructor
- C) Incorrect: Although essential for polymorphism, this is just and overridden function
- D) Correct: the peep object could call the print function in either a Person object or an Airman object making it polymorphic

[**<- Back To README**](../../../README.md)

