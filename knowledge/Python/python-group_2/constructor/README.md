```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## What is occurring on line 2?

KSATs: K0038

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| A class is created | A constructor function is being overloaded | ***The initialization function of a constructor is being defined*** | The self actualization function of the object is called |


Feedback:

- The initialization function for a constructor is defined with: def \_\_init\_\_

[**<- Back To README**](../../../README.md)

