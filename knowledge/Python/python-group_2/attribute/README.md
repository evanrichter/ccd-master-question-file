```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## What is self\.name in the Person class?

KSATs: K0040, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Attribute*** | Constructor | Class | Function |


Feedback:

- A) Correct: An attribute in a class is defined with the self\. identifier
- B) Incorrect: An constructor is as an \_\_init\_\_ function
- C) Incorrect: A class is defined with the keyword class
- D) Incorrect: A function is defined with the keyword def

[**<- Back To README**](../../../README.md)

