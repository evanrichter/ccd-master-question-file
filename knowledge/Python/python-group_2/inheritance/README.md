```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## What is being implemented on line 8?

KSATs: K0030, K0036, K0690

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| Polymorphism | ***Inheritance*** | A list | Overloading |


Feedback:

- The Airman class is inheriting from the Person class with this syntax  Airman\(Person\)

[**<- Back To README**](../../../README.md)

