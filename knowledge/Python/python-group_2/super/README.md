```python
1 |   class Person:
2 |     def __init__(self, name):
3 |         self.name = name
4 |
5 |     def print(self):
6 |         print(self.name)
7 |
8 |   class Airman(Person):
9 |     def __init__(self, name, rank):
10 |         super().__init__(name)
11 |         self.rank = rank
12 |
13 |     def print(self):
14 |         print(self.rank, self.name)  
15 |
16 |   people = {Person('Joe Schmoe'), Airman('Joe', 'SSgt')}
17 |
18 |   for peep in people:
19 |     peep.print()
```

## What keyword in the subclass is used to call a superclass function?

KSATs: K0037, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Person | self | ***super*** | self\.name |


Feedback:

- The super keyword is used to invoked a super class' constructor initialization function

[**<- Back To README**](../../../README.md)

