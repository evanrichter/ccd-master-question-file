```python
1 | x = 4
2 | y = 2
3 | print(x == 4 and y == 2)

```

## In the above Python snippet, what is printed?

KSATs: K0035, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| 4 2 | False | ***True*** | x == 4 and y == 2 |


Feedback:

- Since == is a comparison operator, the value of x will be compared to 4 and the value of y will be compared to 2\. Then, the 'and' logical operator will compare the results of the two comparisons\. Therefore, 4 == 4 is True and 2 == 2 is True; since both sides of the 'and' operator are True, the whole statement becomes True and the word True is printed\.

[**<- Back To README**](../../README.md)

