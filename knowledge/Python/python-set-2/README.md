```python
1   |  dogs = {'Yorkie','Terrier','Doberman','Yorkie'}

```

## What is true about the container created in the Python snippet above?

KSATs: K0017, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| It is a List of size 4 | It is a Set of size 4 | ***It is a Set of size 3*** | It is a Tuple of size 4 |


Feedback:

- Sets are declared with \{\}, there are no duplicate items in a set so there is only one instance of 'Yorkie' in the Set

[**<- Back To README**](../../README.md)

