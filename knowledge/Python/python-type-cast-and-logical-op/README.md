```python
 1 | x= 10
 2 |
 3 | y = 8.76
 4 |
 5 | y = int(y)
 6 |
 7 | z = y == 9 and x == 10

```

## In the above Python snippet, what is the value of z?

KSATs: K0008, K0690, K0712, K0715, K0714, K0735

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| 9 | 10 | ***False*** | True |


Feedback:

- The boolean expression evaluates to False because y is 8 And x is 10\. One comparison is False so the AND expression evaluates to False\.

[**<- Back To README**](../../README.md)

