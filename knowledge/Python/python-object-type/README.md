```python
1 | class Dog:
2 |     def __init__(self, name):
3 |         self.name = name 
4 |  
5 | myDog = Dog("pepe")
6 | 
```

## In the above python snippet, what is myDog?

KSATs: K0033, K0034, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***object*** | class | attribute | constructor |


Feedback:

- Dog is a class\.  myDog is an object or instance of the Dog class

[**<- Back To README**](../../README.md)

