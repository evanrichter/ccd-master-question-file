## In Python, what object is unordered, unique \(no duplicate values\) and immutable\.

KSATs: K0016, K0018, K0014, K0019, K0015, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| list | tuple | ***set*** | dictionary |


Feedback:

- A) Incorrect: A list is mutable and can have duplicate values\.
- B) Incorrect: A tuple can have duplicate values\.
- C) Correct: A set is unordered, unique, and immutable\.
- D) Incorrect: A dictionary is mutable\.

[**<- Back To README**](../../README.md)

