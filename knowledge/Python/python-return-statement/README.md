## What happens when a return statement is executed in a Python function?

KSATs: K0007, K0690

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| It returns execution to the top of the function\. | It returns execution to the beginning of the script\. | ***It terminates the function's execution\.*** | It returns the function's address to the caller\. |


Feedback:

- Executing the return statement in a function will immediately terminate the function's execution\.

[**<- Back To README**](../../README.md)

