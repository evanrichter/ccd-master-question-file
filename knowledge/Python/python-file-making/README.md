```python
1 |def reverse_file(filename):
2 |  fd = os.open(filename)
3 |  contents = fd.read()
4 |  os.remove(filename)
5 |  rfd = os.open('reverse.txt', 'w')
6 |  rfd.write(contents[::-1])
7 |  with open(filename) as fd:
8 |    return fd.read()

```

## In the above image, which line of code contains an issue preventing the function from reading and then deleting a file and writing its contents in reverse to a new file?

KSATs: K0690, K0741, K0742, K0743

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| line 3 | ***line 4*** | line 5 | line 6 |


Feedback:

- Line 4 is attempting to remove a file that has not been closed yet\.

[**<- Back To README**](../../README.md)

