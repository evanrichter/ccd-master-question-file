```python
1 | def do_files_1(filename):
2 |    fd = os.open(filename)
3 |    return fd.read()
4 |
5 |
6 | def do_files_2(filename):
7 |    with open(filename) as fd:
8 |       return fd.read()
9 | 
```

## What is the difference between the above functions?

KSATs: K0690, K0741

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| There is no difference between the two functions | The first functions only returns a file descriptor | ***The second function closes the file*** | The first function requires the absolute path while the second requires the relative path |


Feedback:

- The with context in the second function will automatically close the file when its scope is exited\. even though there is a return function inside it the file will be closed before the function is exited since the with is no longer in scope\. Conversely, os\.open\(\) will not close the file descriptor and it will remain open until os\.close\(\) is called\.

[**<- Back To README**](../../README.md)

