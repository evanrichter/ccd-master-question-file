## How would you find the size of a file in bytes?

KSATs: K0690, K0744

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| os\.len\('example\.txt'\) | os\.size\('example\.txt'\) | os\.sizeof\('example\.txt'\) | ***os\.stat\('example\.txt'\)\.st\_size*** |


Feedback:

- the st\_size attribute in the value returned by os\.stat contains the file size

[**<- Back To README**](../../README.md)

