## Which of the following is NOT a term associated with a linked list?

KSATs: K0056, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Index*** | Head | Node | Next |


Feedback:

- A) Index is a term used for arrays
- B) Head is typically a reference to the head of a linked\-list
- C) Nodes are what is used to create each node in a linked\-list
- D) Next is a standard term used in a linked\-list to refer to either another Node or Null

[**<- Back To README**](../../README.md)

