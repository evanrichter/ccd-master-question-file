```python
1 | S = [['him', 'sell'], [90, 28, 43]]   
2 |    print(S[0][1][1])
3 | 
```

## What is the output for the above Python snippet?

KSATs: K0015, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***e*** | i | 90 | h |


Feedback:

- s\[0\] references the first element in the list which is a list: \['him', 'sell'\], the next \[1\] references the second element in the s\[0\] list: 'sell', \[1\] again reference the 2nd letter in sell

[**<- Back To README**](../../README.md)

