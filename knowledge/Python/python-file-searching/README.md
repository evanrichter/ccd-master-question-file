## Which line of code would you use to access the 16th byte in a file so that it can be overwritten?

KSATs: K0690, K0745

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| fd\[16\] | fd\.read\(16\) | fd\.move\(16\) | ***fd\.seek\(16\)*** |


Feedback:

- A) Incorrect\. You can not use \[\] with a file descriptor to move through a file\.
- B) Incorrect\. Passing an argument to read tells it how many bytes to read, not where\.
- C) Incorrect\. Move is not a file descriptor function\.
- D) Correct\. You use seek to move within the file to the byte specified\. It can then be read from the specified location\.

[**<- Back To README**](../../README.md)

