## In python, which of the following object types are mutable\.

KSATs: K0016, K0018, K0019, K0015, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***list*** | tuple | int | string |


Feedback:

- tuples, ints, and strings are immutable while a list is mutable\.

[**<- Back To README**](../../README.md)

