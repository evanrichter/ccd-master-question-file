```python
1 | def printStuff(stuff):
2 |     print (stuff)
3 | 
```

## Given the Python function defined above, which of the following are valid calls to the function?

KSATs: K0008, K0010, K0009, K0690

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| printStuff\(34\) | printStuff\("Hello"\) | printStuff\(True\) | ***all the above are valid calls*** |


Feedback:

- Since the input parameter, stuff, is not restricted to a specific type, any type can be passed to the function\. Therefore, all options are correct\.

[**<- Back To README**](../../README.md)

