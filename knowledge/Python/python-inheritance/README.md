## In Object\-oriented programming, a sub class is created using a super class via \_\_\_\_\_\_\_\_\_\_\_\_\_\_?

KSATs: K0036, K0690

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Inheritance*** | Extending | Overriding | Casting |


Feedback:

- A) Correct: Inheritance is the term used for a super\-class, sub\-class relationship
- B) Incorrect: Extend is a keyword used in Java to create a sub\-class
- C) Incorrect: Overriding is the term used when a sub class has the same function signatures as a function in its super class
- D) Incorrect: Casting is the term used to data from one type to another\.

[**<- Back To README**](../../README.md)

