## Which of these is not an application layer protocol?

KSATs: K0064, K0066, K0612, K0614

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| HTTP | SMTP | FTP | ***TCP*** |


Feedback:

- A) Incorrect: HTTP is a application protocol for distributed, collaborative, hypermedia information systems\.
- B) Incorrect: SMTP is a application protocol for electronic mail transmission\.
- C) Incorrect: FTP is a application for the transfer of computer files between a client and server on a computer network\.
- D) Correct: TCP is transport layer protocol\.

[**<- Back To README**](../../README.md)

