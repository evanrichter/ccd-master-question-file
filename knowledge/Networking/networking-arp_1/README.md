## This protocol is similar to DNS because of the way it maps layer 3 to layer 2 addresses\.

KSATs: K0066, K0612, K0611, K0613

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| UDP | HTTP | ***ARP*** | IPV4 |


Feedback:

- A) Incorrect: UDP \(User Datagram Protocol\) is a communication protocol
- B) Incorrect: Hypertext Transfer Protocol \(HTTP\) is an application protocol
- C) Correct: Address Resolution Protocol \(ARP\) is a procedure for mapping an Internet Protocol \(IP\) address to a media access control \(MAC\) address
- D) Incorrect: Internet Protocol Version 4 \(IPv4\) is a widely used protocol in data communication over different kinds of networks\.

[**<- Back To README**](../../README.md)

