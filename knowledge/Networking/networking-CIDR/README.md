## Which of the following describes Classless Inter\-Domain Routing \(CIDR\) notation:

KSATs: K0088

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| The process where a network device assigns a public address to one or more computers inside a private network | ***An IP addressing scheme that improves the allocation of IP addresses\. It replaces the old system based on classes A, B, and C*** | A method to dynamically assign an IP address and other network configuration parameters to each device on a network so they can communicate with other IP networks | A communication protocol used for discovering the link layer address, such as a MAC address, associated with a given internet layer address |


Feedback:

- A) Incorrect: This describes NAT\.
- B) Correct
- C) Incorrect: This describes DHCP\.
- D) Incorrect: This describes ARP

[**<- Back To README**](../../README.md)

