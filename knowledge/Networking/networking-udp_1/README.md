## Compared to protocols like DNS or DHCP, UDP is different because \_\_\_\.

KSATs: K0065, K0066, K0613, K0616

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***it operates at the Transport layer of the Open Systems Interconnection \(OSI\) model*** | it operates at the Application layer of the OSI model | it can do the work of both DNS and DHCP | it operates at the Network layer of the OSI model |


Feedback:

- UDP operates at the Transport layer of the OSI model and is a connection\-less protocol because it sends data without guarantee of delivery\. DNS and DHCP are both Application layer protocols that may use UDP for communication\. DNS resolves domain names to IP addresses and DHCP manages a pool of IP addresses by leasing IPs to requesting nodes\.

[**<- Back To README**](../../README.md)

