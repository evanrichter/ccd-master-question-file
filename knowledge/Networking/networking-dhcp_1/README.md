## Upon bootup, a particular network device was configured with 0\.0\.0\.0 so it asked for and received an IP from the \_\_\_\.

KSATs: K0089, K0611, K0613, K0616

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| Router | DNS Server | ARP Server | ***DHCP server*** |


Feedback:

- A) Incorrect: A router is a device that forwards data packets along networks; it doesn't assign IP addresses\.
- B) Incorrect: A DNS Server resolves domain names to IP addresses\.
- C) Incorrect: ARP Server will map a dynamic Internet Protocol address \(IP address\) to a permanent physical machine address in a local area network \(LAN\)/\]\.
- D) Correct: A DHCP Server is a network server that automatically provides and assigns IP addresses, default gateways and other network parameters to client devices\. It relies on the standard protocol known as Dynamic Host Configuration Protocol or DHCP to respond to broadcast queries by clients\.

[**<- Back To README**](../../README.md)

