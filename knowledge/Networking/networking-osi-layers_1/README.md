## One difference between a switch and a hub is that the hub operates at the \_\_\_ layer of the Open Systems Interconnection \(OSI\) model\.

KSATs: K0066, K0089

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Physical*** | Network | Data Link | Transport |


Feedback:

- A hub operates at the physical layer because it only passes data\. A switch actually processes packets and operates at either the data link or network layers\.

[**<- Back To README**](../../README.md)

