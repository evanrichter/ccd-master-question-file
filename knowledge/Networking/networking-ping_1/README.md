## The ping command uses what protocol?

KSATs: K0615

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***ICMP*** | HTTP | UDP | ARP |


Feedback:

- A) Correct: ICMP sends a small packet of information containing an ICMP ECHO\_REQUEST to a specified computer, which then sends an ECHO\_REPLY packet in return\.
- B) Incorrect: HTTP is the underlying protocol used by the World Wide Web and this protocol defines how messages are formatted and transmitted\.
- C) Incorrect: UDP is an alternative communications protocol to TCP used primarily for establishing low\-latency and loss\-tolerating connections between applications on the internet\.
- D) Incorrect: ARP is a protocol used by the Internet Protocol \(IP\), specifically IPv4, to map IP network addresses to the hardware addresses used by a data link protocol\.

[**<- Back To README**](../../README.md)

