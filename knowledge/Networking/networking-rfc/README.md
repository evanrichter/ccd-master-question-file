## What type of document is authored by engineers and computer scientists in the form of a memorandum describing methods, behaviors, research, or innovations applicable to the working of the Internet and Internet\-connected systems?

KSATs: K0617

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| IEEE \- Instructions of Electrical and Electronics Engineers | ***RFC \- Request for Comments*** | W3C \- World Wide Web Comments | NGM \- Networking Guidance Memorandum |


Feedback:

- An RFC is authored by engineers and computer scientists in the form of a memorandum describing methods, behaviors, research, or innovations applicable to the working of the Internet and Internet\-connected systems\. It is submitted either for peer review or to convey new concepts, information, or occasionally engineering humor\.

[**<- Back To README**](../../README.md)

