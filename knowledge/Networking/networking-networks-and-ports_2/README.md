## In networking and typically reserved for servers, which port numbers are considered "privileged" ports?

KSATs: K0610

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***< 1024*** | < 2048 | > = 1024 | > = 2048 |


Feedback:

- A) Correct: Ports below 1024 are special in that normal users are not allowed to run servers on them\.
- B) Incorrect: Although some ports of this range are privileged only ports below 1024 are privileged ports\.
- C) Incorrect: Only ports below 1024 are privileged\.
- D) Incorrect: Only ports below 1024 are privileged\.

[**<- Back To README**](../../README.md)

