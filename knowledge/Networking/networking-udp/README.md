## Which of the following is correct about the UDP protocol?

KSATs: K0065

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| Keeps track of lost packets | Adds sequence numbers | Checks packet arrival order | ***Faster than TCP*** |


Feedback:

- UDP is faster than TCP because it doesn't add sequence numbers, doesn't try to keep track of lost packets, and doesn't check for arrival order like TCP

[**<- Back To README**](../../README.md)

