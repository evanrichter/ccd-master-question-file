## The first \_\_\_ byte\(s\) of a physical machine address, also known as a/an \_\_\_\_\_ address, identify the manufacturer\.

KSATs: K0086

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| 1, IP | 2, DHCP | ***3, MAC*** | 2, MAC |


Feedback:

- A physical machine address is also known as a Media Access Control \(MAC\) address and the first three bytes identify the manufacturer\.

[**<- Back To README**](../../README.md)

