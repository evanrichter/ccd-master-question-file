## Which of the following describe OSI layer 7?

KSATs: K0075

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| The data link layer that is responsible for the node to node delivery of the message\. The main function of this layer is to make sure data transfer is error\-free from one node to another, over the physical layer\. | The network layer that transmits data from one host to the other located in different networks\. It also takes care of packet routing i\.e\. selection of the shortest path to transmit the packet, from the number of routes available | The transport layer that provides services to application layer and takes services from network layer\. The data in the transport layer is referred to as Segments | ***The application layer which is implemented by the network applications\. These applications produce the data, which has to be transferred over the network\.*** |


Feedback:

- A) Incorrect: This is layer 2\.
- B) Incorrect: This is layer 3\.
- C) Incorrect: This is layer 4\.
- D) Correct: this is layer 7\.

[**<- Back To README**](../../README.md)

