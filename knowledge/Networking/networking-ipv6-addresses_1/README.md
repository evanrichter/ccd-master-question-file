## The size of an IPv6 address differs from an IPv4 address because it has \_\_\_ compared to IPv4's \_\_\_?

KSATs: K0082, K0081

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| 32 bits, 128 bits | ***128 bits, 32 bits*** | 64 bits, 32 bits | 100 bits, 64 bits |


Feedback:

- An IPv6 address has 128 bits, 16 bytes X 8 bits, and an IPv4 address has 32 bits, 4 bytes X 8 bits\.

[**<- Back To README**](../../README.md)

