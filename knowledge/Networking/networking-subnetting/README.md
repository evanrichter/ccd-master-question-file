## What is one reason for network subnetting?

KSATs: K0072

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***It helps relieve network congestion\.*** | It provides every organization an equal number of IP addresses\. | It allows IP octets above 255 | It reduces the need for Domain Name System \(DNS\) servers\. |


Feedback:

- Subnetting ensures that traffic destined for a device within a subnet stays in that subnet, which reduces congestion\. Strategic placement of subnets reduces a network’s load and enables efficient traffic routing\.

[**<- Back To README**](../../README.md)

