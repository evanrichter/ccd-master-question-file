## In networking, which number type starts at the random number chosen during the handshake, is cumulative and is used to maintain accountability of all packets?

KSATs: K0064

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Sequence*** | Reserved | Nonce Sum | Acknowledgement |


Feedback:

- A) Correct: A sequence number is a number assigned to data fragments by Stream Control Transmission Protocol \(SCTP\)\.
- B) Incorrect: A reserved number is typically used in the port assignment\.
- C) Incorrect: A nonce sum is a sign to designate error between ECN\-nonce supporting endpoints\.
- D) Incorrect: A acknowledgment number type identifies the byte in the stream of data from the sending TCP to the receiving TCP that the first byte of data in this segment represents\.

[**<- Back To README**](../../README.md)

