## Which of the following Python functions enables data transfer?

KSATs: K0068, K0618

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| bind\(\) | socket\(\) | connect\(\) | ***send\(\)*** |


Feedback:

- A) Incorrect: bind\(\) binds a socket to the address\.
- B) Incorrect: socket\(\) creates a new socket using the given address family\.
- C) Incorrect: connect\(\) connects to a remote socket at address\.
- D) Correct: send\(\) sends data to the socket\.

[**<- Back To README**](../../README.md)

