## Which of the following describes a Centralized Workflow?

KSATs: K0001

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Flow that uses a central repository to serve as the single point\-of\-entry for all changes to the project\.*** | Flow where all feature development should take place in a dedicated branch instead of the master branch\. | Flow that assigns very specific roles to different branches and defines how and when they should interact\. | Flow where instead of using a single server\-side repository, it gives every developer their own server\-side repository\. |


Feedback:

- A) Correct: the Centralized Workflow uses a central repository to serve as the single point\-of\-entry for all changes to the project\. Instead of trunk, the default development branch is called master and all changes are committed into this branch\. This workflow doesnâ€™t require any other branches besides master
- B) Incorrect: this describes a Feature Branch workflow
- C) Incorrect: this describes a GitFlow Workflow
- D) Incorrect: this describes a Forking Workflow

[**<- Back To README**](../../README.md)

