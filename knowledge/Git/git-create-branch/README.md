## Which of the following examples is the proper way to create and check out a branch in Git?

KSATs: K0722

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| git create \-b my\-branch | git checkout my\-branch | ***git checkout \-b my\-branch*** | git branch \-n my\-branch |


Feedback:

- The \-b command allows you to create a new branch with checkout\. Doing a checkout without \-b checks out an existing branch\.

[**<- Back To README**](../../README.md)

