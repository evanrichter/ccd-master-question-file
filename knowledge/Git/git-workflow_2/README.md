## Which of the following describes a GitFlow Workflow?

KSATs: K0001

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| Flow that uses a central repository to serve as the single point\-of\-entry for all changes to the project | Flow where all feature development should take place in a dedicated branch instead of the master branch\. | ***Flow that assigns very specific roles to different branches and defines how and when they should interact\.*** | Flow where instead of using a single server\-side repository, it gives every developer their own server\-side repository\. |


Feedback:

- A) Incorrect: this describes a Centralized Workflow
- B) Incorrect: this describes a Feature Branch workflow
- C) Correct: Gitflow is ideally suited for projects that have a scheduled release cycle\. This workflow doesnâ€™t add any new concepts or commands beyond whatâ€™s required for the Feature Branch Workflow\. Instead, it assigns very specific roles to different branches and defines how and when they should interact\. In addition to feature branches, it uses individual branches for preparing, maintaining, and recording releases
- D) Incorrect: this describes a Forking Workflow

[**<- Back To README**](../../README.md)

