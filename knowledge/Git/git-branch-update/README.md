## Assume you have made changes to local files and you want to update your local branch with those files, which git commands would you use?

KSATs: K0724

## Answer
| A | ***B*** | C | D |
| :--- | :--- | :--- | :--- |
| save and update | ***add and commit*** | add and push | save and commit |


Feedback:

- The add command will stage the changes for the next commit\. Add can be done for a specific file or an entire directory\.  Commit replaces the last commit with the staged changes and last commit combined\.

[**<- Back To README**](../../README.md)

