## What is one purpose of a Git repository?

KSATs: K0720

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| It validates logic in your programs\. | It provides syntax checking, compiles, and links programs | ***It provides version control for project files\.*** | It creates automated unit tests stubs from pydoc documents |


Feedback:

- One major benefit of Git is that it provides version control for all project documents\.

[**<- Back To README**](../../README.md)

