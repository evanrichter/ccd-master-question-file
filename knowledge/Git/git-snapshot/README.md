## Which of the following commands would be used to snapshot a Git repository?

KSATs: K0721

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***'git add' followed by a 'git commit'*** | 'git commit' followed by a 'git snapshot' | 'git commit' followed by a 'git tag' | 'git revision' followed by a 'git push' |


Feedback:

- A snapshot of a repository is essentially just a revision of the branch, each time you do an add and commit you create a 'snapshot' of the branch\.

[**<- Back To README**](../../README.md)

