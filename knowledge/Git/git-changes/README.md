## What does the 'git diff' command do?

KSATs: K0725

## Answer
| ***A*** | B | C | D |
| :--- | :--- | :--- | :--- |
| ***Shows unstaged changes between your branch and working directory\.*** | Shows unstaged changes between your branch and previous version of branch\. | Shows changes since last commit\. | Shows changes between different master branch versions\. |


Feedback:

- The git diff command shows unstaged changes between your branch and working directory\.

[**<- Back To README**](../../README.md)

