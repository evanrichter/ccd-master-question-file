## If you are in the Master branch and want to merge in a branch called 'add\_git\_questions', what command would you use?

KSATs: K0723

## Answer
| A | B | ***C*** | D |
| :--- | :--- | :--- | :--- |
| git commit add\_git\_questions | git merge master | ***git merge add\_git\_questions*** | git master merge add\_git\_questions |


Feedback:

- The git merge command expects the name of an existing branch and will attempt to merge that branch into the branch where the command is being executed\.

[**<- Back To README**](../../README.md)

