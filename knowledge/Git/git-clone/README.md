## What does the command 'git clone https://myRepo' do?

KSATs: K0726

## Answer
| A | B | C | ***D*** |
| :--- | :--- | :--- | :--- |
| Creates a new repo at https://myRepo\. | Clones the master repo to the server located at https://myRepo\. | Clones the repo located at https://myRepo onto remote machine\. | ***Clones the repo located at https://myRepo onto local machine\.*** |


Feedback:

- The clone command will create a copy of the repo to the local machine where the command is executed\.\.

[**<- Back To README**](../../README.md)

