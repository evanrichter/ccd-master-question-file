# Cyber Capability Developer

This repo is used for studying the basic developer evaluation

- [CCD Handout](https://90cos.gitlab.io/cyt/training-systems/Exam_Provisioning_System/handouts/CCD-Basic/Handout-Perform-Basic.html)
- To set up a replica exam environment for the performance questions, see
the setup/installation instructions <[here](https://gitlab.com/90cos/public/evaluations/staneval-environment-configuration)>.

Consider converting to a contributing.md