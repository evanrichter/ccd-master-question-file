#!/usr/bin/env python

import unittest, subprocess, os, re, base64, xmlrunner
from testfile import *
import tempfile

class CheckRideTestOne(unittest.TestCase):

	def test_get_text_files(self):
		test_file_dat = [
			"Hello.",
			"45 data items",
			"multi\nline\nfile"
		]
		output = dir_reader("test_directory")
		self.assertTrue(output)
		for k in output.keys():
			self.assertTrue(output.get(k) in test_file_dat)


if __name__ == '__main__':
	#unittest.main()
    with open('unittest.xml', 'w') as output:
      unittest.main(
      testRunner=xmlrunner.XMLTestRunner(output=output), 
      failfast=False, 
      buffer=False, 
      catchbreak=False
      )

