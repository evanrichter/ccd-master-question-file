"""
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0024 - Declare and/or implement container data type.
	S0023 - Declare and implement data types.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0080 - Demonstrate the skill to implement exception handling.
	S0079 - Validate expected input.
	S0110 - Implement error handling.
	S0082 - Implement conditional control flow constructs.
"""
'''
For this problem, assume today is July 4, 2019. Write a function that receives a year, month, day
representing a person's birthdate. Depending on the person's age, you will build a set of
eligibilities that person has. If the person is:

< 16  "Ineligible"
>= 16 "Drive"
>= 18 "Vote"
>= 21 "Drink"
>= 55 "Discount"
>= 67 "Retire"

For example, if a person is less than 16 the set will contain:  {"Ineligible"}
             if a person is 33, the set will contain:{"Drive","Vote","Drink"}

If an invalid year, month, or day is received as a parameter, or the passed date
is greater than July 4, 2019 return an empty set.

'''


def getEligibility(year, mon, day):
    return ''
            
