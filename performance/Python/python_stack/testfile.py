"""
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0382 - Create a class constructor or destructor
	S0024 - Declare and/or implement container data type.
	S0023 - Declare and implement data types.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0078 - Push and pop a Stack.
	S0062 - Create and destroy a Stack.
	S0081 - Implement a looping construct.
	S0082 - Implement conditional control flow constructs.
"""
'''
This task will implement a simple stack class using a linked list. A numNode class is defined below
to facilitate the creation of a stack. Each node in the stack contains an integer.

Using the numNode class, write the class stack with the following attributes, functions and functionality:

Attributes:
   top = a reference to the top of the stack
   size = the number of items in the stack

pushStack - This function receives one parameter. A list of numbers
            to push on the stack. The function should iterate the list
            and push each number on to the stack as it's iterated incrementing
			the size of the stack each time.

popStack - This function receives one parameter. An integer indicating
           how many items to pop from the stack. If the integer
           passed is greater than the number of items in the stack
           the function will return "STACK TOO SMALL" and will not
           modify the stack.
           Otherwise, the function will pop the number of items from
           the stack and return a list of items that
           were removed from the stack. The list will have the items
           in the order they were removed. This will also decrement the size
		   of the stack for each item that is popped.

emptyStack - This function has no parameters and simply empties the
             stack.

'''


class numNode:
    def __init__(self, num):
        self.num = num
        self.next = None


class stack:
    def __init__(self):


    def pushStack(self, items):


    def popStack(self, numPops):


    def emptyStack(self):

