	K0767 - Understand move instructions.
	K0778 - Understand mathematical instructions.
	K0786 - Understand logical instructions.
	K0798 - Understand string instructions.
	K0794 - Understand the purpose of the cmp instruction.
	K0797 - Understand the purpose of the loop instruction.
	K0796 - Understand the purpose of the jcc and other conditional jump instructions.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0117 - Utilize labels for control flow.
	S0125 - Utilize general purpose instructions.
	S0118 - Implement conditional control flow.
	S0129 - Utilize common string instructions.
	S0141 - Execute instructions using raw opcodes
	S0136 - Utilize x86 calling conventions
	S0134 - Utilize registers and sub-registers
	S0177 - Write assembly code that accomplishes the same functionality as a given a high level language (C or Python) code snippet.
