bits 32

section .text

global _character_replace

; 
; Given a string, replace all instances of one character with another.
;
; char* __cdecl character_replace(char* oldString, char* newString, char oldChar, char replaceChar)
_character_replace:
	push ebp
	mov ebp, esp

	; Stack addresses of interest
	; ebp + 0x08 original (input) string
	; ebp + 0x0C finished (output) string
	; ebp + 0x10 original character
	; ebp + 0x14 replacement character

	;; Save ebx
	push ebx

	;; Clear registers
	xor eax, eax
	xor ebx, ebx
	xor edx, edx

	;; Place to hold search char and replacement
	mov bl, [ebp+0x10]
	mov dl, [ebp+0x14]
	
	;; Set up for string instructions
	mov esi, [ebp+0x08]
	mov edi, [ebp+0x0c]

copy:
	mov al, [esi]
	test eax, eax
	mov [edi], al
	jz done

	cmp bl, al
	jne skip
	mov [edi], dl
skip:		
	inc edi
	inc esi
	jmp copy

done:
	pop ebx

	mov eax, [ebp+0x0c]

	mov esp, ebp
	pop ebp

	ret
