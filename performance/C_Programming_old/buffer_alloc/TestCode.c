/*
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0030 - Utilize bitwise operators to manipulate binary values.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0035 - Declare and/or implement of arrays and multi-dimensional arrays.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0033 - Utilize assignment operators to update a variable.
	S0042 - Open and close an existing file.
	S0043 - Read, parse, write (append, insert, modify) file data.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0079 - Validate expected input.
	S0090 - Allocate memory on the heap (malloc).
	S0097 - Create and use pointers.
	S0091 - Unallocating memory from the heap (free).
	S0081 - Implement a looping construct.
	S0108 - Utilize post and pre increment/decrement operators.
	S0110 - Implement error handling.
	S0082 - Implement conditional control flow constructs.
*/
#include <Windows.h>
#include <stdio.h>
#include "TestCode.h"


/*
  (part one): This task will involve allocating a buffer
				of (sizeOfData) bytes, and reading up to that amount
				into it from the file specified in 'filename'. Additionally,
				the data in the file has been pseudo-encrypted (poorly) with a single-
				byte XOR, which you will be required to undo (the byte that the buffer
				was XOR'd against will be provided in 'key'). Finally, assuming the task
				was successful, (and all pointers are valid) the pointer to the resulting 
				buffer should be stored in the area referenced by buffPtr.

 Expected Return Values:
			- task was completed successfully: ERROR_SUCCESS (0)
			- bad input was provided: ERROR_INVALID_PARAMETER (87)
			- you were unable to allocate enough memory: ERROR_OUTOFMEMORY (14)
			- you were unable to open the file: ERROR_OPEN_FAILED (110)
*/
int decodeFromFile(char* filename, unsigned int sizeOfData, unsigned char key, void** buffPtr)
{

	return 0;
}

/*
 (part two): The second part of the task; this simply requires you
				  to appropriately free the buffer allocated in the last task
				  (if possible).

 Expected Return Values:
			- the task completed successfully: 1  (true)
			- the task failed/bad input: 0 (false)
*/
int freeDecodedBuffer(void* buffer)
{
	int out = 0;

	return out;
}