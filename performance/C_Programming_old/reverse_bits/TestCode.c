/*
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0030 - Utilize bitwise operators to manipulate binary values.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0035 - Declare and/or implement of arrays and multi-dimensional arrays.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0081 - Implement a looping construct.
	S0108 - Utilize post and pre increment/decrement operators.
	S0082 - Implement conditional control flow constructs.
	S0160 - Utilize the standard library.
*/
#include <Windows.h>
#include <stdio.h>
#include "TestCode.h"

/*
The function reverseBits reverses the order of the bits in an unsigned int value.
The function takes an input value and returns a string representing the bits in reverse order.

Example:
Input value 12 (0000 0000 0000 0000 0000 0000 0000 1100)
Output value "00110000000000000000000000000000"

*/

char* reverseBits(unsigned int value)
{
    char *temp; 

    return temp;
}