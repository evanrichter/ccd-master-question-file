#pragma once

#include <Windows.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
	// Task One
	char* reverseBits(unsigned int value);

#ifdef __cplusplus
}
#endif