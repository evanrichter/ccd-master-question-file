/*
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0030 - Utilize bitwise operators to manipulate binary values.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0053 - Implement a function that returns a memory reference.
	S0048 - Implement a function that receives input parameters.
	S0090 - Allocate memory on the heap (malloc).
	S0097 - Create and use pointers.
	S0160 - Utilize the standard library.
*/
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "TestCode.h"

/*
The function packCharacters packs four character values into a four-byte unsigned int variable.
The first character will be placed in the first byte of the int variable, the second character in the second byte,
the third character in the third byte, and the fourth character in the fourth byte 
So for ch1=a, ch2=b, ch3=c, ch4=d, the returning int = abcd

The function char* unpackCharacters(unsigned int pack) to unpack characters from a four-byte unsigned int.
The unpackCharacters function returns a pointer the unpacked characters.
The first character in the array will contain the fourth byte of the unsigned int
so for int pack = abcd, the returning char * = {d,c,b,a}

*/


unsigned int packCharacters(char ch4, char ch3, char ch2, char ch1) 
{

   return 0;
}


char* unpackCharacters(unsigned int pack) 
{

	char * temp;

   
	return temp;
   
}


