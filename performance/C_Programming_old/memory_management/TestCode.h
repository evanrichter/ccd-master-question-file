#pragma once

#include <Windows.h>
#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif
	char* makeAndPrintSentences(char **, int);
#ifdef __cplusplus
}
#endif
