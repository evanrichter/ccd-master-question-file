/*
This question is intended to evaluate the following topics:
	A0082 - Identify and eliminate memory leaks.
	A0018 - Analyze a problem to formulate a software solution.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
	S0016 - Debug a C program.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0036 - Declare and implement a char * array (string).
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0090 - Allocate memory on the heap (malloc).
	S0091 - Unallocating memory from the heap (free).
	S0081 - Implement a looping construct.
	S0082 - Implement conditional control flow constructs.
	S0147 - Remediate formatting string vulnerabilities.
	S0148 - Implement safe buffer size allocation
*/
/******************************************************************************

The function makeAndPrintSentence accepts a character pointer to a char pointer array
representing multiple sentences, and an int representing the number of sentences.
Each of the provide raw sentences has no spacing or punctuation.
Each capital letter in the sentence represents a start of a word. The function 
is parsing the string and creating and printing a new sentence with spaces in between 
each word with only the first word capitalized. It also puts a period at the end of
the sentence.

For example, the raw provided sentence may be:  MyDogIsBig
and, what will print is:                        My dog is big.

Task #1

There are several problems related to memory management in this solution.  
You need to identify and correct those memory problems so the function 
still executes properly.

Task #2

Add some code to the makeAndPrintSentence function such that if the first word of
a sentence begins with the word: 'Who', 'What', 'Where', 'When', 'Why', or 'How', 
then the sentence ends with a question mark.

For example, the raw provided sentence may be:  WhatIsYourName
and, what should print is:                      What is your name?

*******************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>


void makeAndPrintSentences(char **sentences, int size)
{
    int len, capCount, j;
    char * newSentence;
    for(int k = 0; k < size; k++)
    {
    	len = strlen(sentences[k]);
    	capCount = 0;
    
        // count number of spaces needed
    	for (int i = 0; i < len; i++)
    	{
    		if (sentences[k][i] >= 'A' && sentences[k][i] <= 'Z')
    			capCount++;
    	}
    
    	newSentence = malloc(len);
    	j = 0;
    	for (int i = 0; i < len; i++, j++)
    	{
    		newSentence[j] = sentences[k][i];
    		if (sentences[k][i] >= 'A' && sentences[k][i] <= 'Z' && i > 0)
    			newSentence[j] += 32;  //change to lowercase
    		if (sentences[k][i+1] >= 'A' && sentences[k][i+1] <= 'Z')
    			newSentence[++j] = ' '; //add space between words
    	}
    	newSentence[j] = '.';
    
    	printf("%s\n",newSentence);
    }
}
