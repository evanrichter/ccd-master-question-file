#include <gmock\gmock.h>
#include <Windows.h>
#include "RunCode.h"
#include "TestCode.h"


TEST(TestCase1, checkMakeSentenceTest)
{
    char *sents[] = { "TheQuickBrownFoxJumpedOverTheLazyDog'sBack",
                      "WhoIsTheMan",
                      "WilliamIsTheSoCalledPrince",
                      "WhatIsYourName",
                      "HowDidYouDoThat"};
                      
    makeAndPrintSentences(sents, 5);	
}

