/*
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0029 - Utilize arithmetic operators (PEMDAS +, -, *, /, %) in mathematical equations.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0036 - Declare and implement a char * array (string).
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0053 - Implement a function that returns a memory reference.
	S0048 - Implement a function that receives input parameters.
	S0079 - Validate expected input.
	S0090 - Allocate memory on the heap (malloc).
	S0097 - Create and use pointers.
	S0081 - Implement a looping construct.
	S0108 - Utilize post and pre increment/decrement operators.
	S0082 - Implement conditional control flow constructs.
	S0160 - Utilize the standard library.
*/
#include <Windows.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "TestCode.h"



/*
Write a function makeParagraph that accepts a character pointer to a char pointer array representing multiple sentences, and an int
representing the number of sentences.  Each sentence is a part of a paragraph. Each sentence is terminated with a single digit 
(from 0 up to 9) representing the order the sentence should appear in the paragraph. The function should return the ordered 
paragraph as a single string with the digits at the end of the sentence replaced with a space. 
However, the last sentence should not have a space after it.

For example, you may be passed the following pointer to pointer array of char:

char **strings = {"What's yours?3", "Hello there!1", "My name is Jimmy.2"};

the function will return the string:  "Hello there! My name is Jimmy. What's yours?"

If any sentence in the array is empty, return the string: "INVALID".

Assume all valid data has the correct and valid sequence of numbers


*/


char * makeParagraph(char **sentences, int size)
{
	char * para;
	return para;
}

