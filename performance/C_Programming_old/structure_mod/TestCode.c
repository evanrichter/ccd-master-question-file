/*
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0030 - Utilize bitwise operators to manipulate binary values.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0053 - Implement a function that returns a memory reference.
	S0048 - Implement a function that receives input parameters.
	S0079 - Validate expected input.
	S0097 - Create and use pointers.
	S0081 - Implement a looping construct.
	S0082 - Implement conditional control flow constructs.
	S0156 - Utilize a struct composite data type.
*/
#include <Windows.h>
#include <stdio.h>
#include "TestCode.h"


/* 
//  This exercise will provide you with four parameters:
//		   - a pointer to a CONTAINER structure (defined in TestCode.h),
//		   - an 'id', which corresponds to the 'id' field of a node in the linked list,
//		   - an 'action' parameter, which will get you your result, and finally 
//		   - a pointer to an integer (result), to store the data you obtain from the requested structure. 
//		     
//			Your task will be to walk along the linked list  the CONTAINER structure startNode points at 
//		   is part of until you locate a node with an id equal to 'id' (or return the appropriate result listed below, 
//		   if a node with that id can't be found) and then do the following:
//
//		   - if the structure is of type 'TypeBitmask', XOR the 
//			 the bitmask field of the contained union against 'action', and store the result of this
//			 operation in the area 'result' points at.
//
//		   - if the structure is instead of type 'TypeFuncPtr', call the function in the embedded union,
//			 providing as its parameter 'action', and store the result in 'result'. (e.g., *result = function(action))
//
// Expected Return Values:
//		   - If any portion of the input field(s) are bad, return ERROR_INVALID_PARAMETER (87).
//		   - If the requested ID can't be found in the list, return ERROR_NOT_FOUND (1168)
//		   - If the operation completes successfully, return ERROR_SUCCESS (0)
*/
int structureMod(PCONTAINER startNode, unsigned long id, unsigned long action, int* result)
{
	    
    return 0;

}