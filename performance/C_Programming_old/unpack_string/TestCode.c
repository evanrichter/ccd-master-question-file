/*
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0030 - Utilize bitwise operators to manipulate binary values.
	S0034 - Declare and implement appropriate data types for program requirements.
	S0031 - Utilize logical operators to formulate boolean expressions.
	S0032 - Utilize relational operators to formulate boolean expressions.
	S0036 - Declare and implement a char * array (string).
	S0033 - Utilize assignment operators to update a variable.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0052 - Implement a function that returns a single value.
	S0048 - Implement a function that receives input parameters.
	S0079 - Validate expected input.
	S0081 - Implement a looping construct.
	S0108 - Utilize post and pre increment/decrement operators.
	S0082 - Implement conditional control flow constructs.
*/
#include <Windows.h>
#include <stdio.h>
#include "TestCode.h"

/*
// This task revolves around decoding a very simply encoded string.
//		   You will be provided with two buffers and their lengths: a buffer containing the encoded string
//		   (packedString), where each value has been left shifted by 1 bit (which, given that ASCII
//		   has 127 allowable ordinal values, still keeps us under the 1-byte boundary!), and outBuffer, where
//		   all of the de-obfuscated output should be stored.
//
// Expected Return Values:
//		- The task is successful: ERROR_SUCCESS (0)
//		- Bad input is provided: ERROR_INVALID_PARAMETER (87)
*/

int unpackString(char* outBuffer, int outBufferLength, unsigned char* packedString, int packedStringLength)
{

    return 0;

}
