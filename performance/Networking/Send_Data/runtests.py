import unittest, xmlrunner
from client import *


class NetworkUnitTest(unittest.TestCase):
    def test_sending_data(self):
        response = send_the_data()
        self.assertTrue(response == b'(b"Cooking MC\'s like a pound of bacon", 88)')

if __name__ == '__main__':
    unittest.main()
