import unittest
from client import *


class NetworkUnitTest(unittest.TestCase):
    def test_rest(self):
        key_val = do_key()
        self.assertEqual(len(key_val),128)
        self.assertTrue(key_val.isalnum())
        response = do_put(key_val)
        self.assertTrue(response == "Success! You found the key\n")

if __name__ == '__main__':
    unittest.main()
