import unittest, xmlrunner
from client import *


class NetworkUnitTest(unittest.TestCase):
    def test_keys(self):
        response = match_keys()
        self.assertTrue(response == b'Success! You found the key!\n')

if __name__ == '__main__':
    unittest.main()
