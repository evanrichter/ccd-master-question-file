"""
This question is intended to evaluate the following topics:
	A0019 - Integrate functionality between multiple software components.
	A0018 - Analyze a problem to formulate a software solution.
	S0026 - Utilize standard library modules.
	S0007 - Skill in writing code in a currently supported programming language (e.g., Java, C++).
	S0047 - Implement a function that returns multiple values.
	S0110 - Implement error handling.
	S0096 - Utilize the Socket library.
"""
import socket
import re
import binascii
from cryptography.fernet import Fernet

####### Instructions:
# You are communicating with a secret agent.
# Your secret agent will only acknowledge you if he recognizes your host name.
# In order to talk to the agent, they will send you a key.
# Using the key they sent, create a Fernet key using the cryptography library.
# In order for both of you to be confident in your communication,
# you need to verify their key by checking the signing key in the Fernet object.
# After confirming that both of you are trusted, you can start decrypting the secret agents messages.
# Randomly a Nation State actor will contact you with an initial fake key,
# if you detect the fake key close your socket immediately.
#
# Your secret agent will only acknowledge you if he recognizes your host name
# 1) Make a function called dowork() that accepts no parameters and does the following:
#    a) Make a connection to the server.
#    b) Find your ip address using gethostname() and send your ip address.
#    c) The server will send you a master key that you will use to make a Fernet key.
#    d) The master key will be passed to a function you create called create_fernet_and_find_sign_key(bytes).
#       - If create_fernet_and_find_sign_key(bytes) returns a ValueError stating 'Potential Nation State Actor 
#         Detected.', the socket will be closed and the error will be returned as a string.
#    e) Send the signing key back to the server to be verified.
#       - If the signing key is valid, the server will send back a spy phrase.
#       - If the signing key is invalid, the server will send "Wrong signing key"
#    f) Use your fernet key to decrypt the spy phrase and send it back to the server to confirm.
#    g) In response to the confirmation, the server will send you a final confirmation message.
#    h) Print the decrypted message to the console.
#    i) Return the decrypted message.
#
# 2) Make a function called create_fernet_and_find_sign_key(bytes) that accepts the master key as the only parameter
#    a) The input parameter should be declared as a bytes object with no default value
#    b) This function will generate the Fernet key object and find its signing key.
#    c) Return both the Fernet key and its signing key.
#    d) If there is an error creating the fernet object, this function should throw a ValueError with the message 
#      'Potential Nation State Actor Detected.'; note, do not use BaseException or Exception when detecting errors.
#
#########################

# HOST = '10.8.0.1' # The server's hostname or IP address when using VPN
HOST = '127.0.0.1' # The server's hostname or IP address
PORT = 5000        # The port used by the server


# Your create_fernet_and_find_sign_key function here

def dowork():
    # Your code here
    return


def main():
    dowork()


if __name__ == '__main__':
    main()
