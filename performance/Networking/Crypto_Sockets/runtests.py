import unittest
from client import *

valid_msg = [
        b"A really secret message. Not for prying eyes.",
        b"Here is the information you need.",
        b"The eagle is in the nest.",
        b"I need to return some video tapes.",
        b"The biscuit has changed hands.",
        b"The spider has many eyes.",
        b"The red fox trots quietly at midnight.",
        b"The asset is invalidated.",
        b"Please direct me to the nearest safe house.",
        "Potential Nation State Actor Detected."
    ]


class NetworkUnitTest(unittest.TestCase):
    def test_crypto(self):
        response = dowork()
        self.assertTrue(response in valid_msg)


if __name__ == '__main__':
    unittest.main()
