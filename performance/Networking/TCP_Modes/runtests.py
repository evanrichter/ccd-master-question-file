import unittest, xmlrunner
from client import *


class NetworkUnitTest(unittest.TestCase):
    def test_sending_data(self):
        recv = query_mode()
        self.assertEqual(hex(recv[0]), '0x800')
        self.assertEqual(recv[1], 12648430)
        response = get_key(recv)
        self.assertTrue(response == 'aa072fbbf5f408d81c78033dd560d4f6')

if __name__ == '__main__':
   unittest.main()
