#pragma once

// #include <Windows.h>
#include <stdio.h>
#include "celsius.h"

#ifdef __cplusplus
extern "C" {
#endif
	// Task One
	int processTemps(int temps[][2], int rows);

#ifdef __cplusplus
}
#endif
